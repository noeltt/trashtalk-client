import { StyleSheet } from 'react-native'
import { mainPaddingLength } from '../utils/constants'

// const blackGradientColor = [
//     {offset: '0%', color: 'black', opacity: '1'},
//     {offset: '70%', color: '#44107A', opacity: '0'}
// ]

export const mainStyles = StyleSheet.create({
    // later base colors, formats, etc
    imageFill: {
        height: '100%',
        width: '100%',
    },
    paddingLeftDefault: {
        paddingLeft: mainPaddingLength,
    },
    svgForceFill: {
        minWidth: 300,
        // minHeight: '100%'
    },
    logoWindow: {
        height: 93,
        width: 80,
        marginTop: '50%',
    },
    titleWindow: {
        height: 60,
        width: 279,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mainScrollView: {
        // alignItems: 'center',
        backgroundColor: 'white',
        // paddingBottom: (footerHeight),
        // height: windowHeight - (footerHeight + devGap)
    },
    mainIAView: {
        alignItems: 'center',
        backgroundColor: 'white',
    },
    textSizeOne: {
        fontSize: 20,
        lineHeight: 28,
        fontStyle: 'normal',
        fontWeight: 'normal',
        color: '#333333',
    },
    textSizeTwo: {
        fontSize: 16,
        lineHeight: 20,
        fontStyle: 'normal',
        fontWeight: 'normal',
        color: '#333333',
    },
    topBorderLine: {
        // marginLeft: -10,
        borderTopWidth: 0.5,
        borderColor: 'lightgray',
    },
    bgLightGray: {
        backgroundColor: '#F2F2F2',
    },
    bgWhite: {
        backgroundColor: 'white',
    },
})
