module.exports = {
    parser: 'babel-eslint',
    env: {
        es2021: true,
        node: true,
    },
    extends: ['plugin:react/recommended', 'standard'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        'react/prop-types': 'off',
        'react/display-name': 'off',
        'no-undef': 'off',
        'prefer-const': 'off',
        'array-callback-return': 'off',
        'no-floating-decimal': 'off',
        'space-before-function-paren': 'off',
        'operator-linebreak': 'off',
        'multiline-ternary': 'off',
        'brace-style': [2, 'stroustrup'],
        indent: ['error', 4],
    },
}
