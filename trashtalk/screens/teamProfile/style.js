/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    textInputHeight,
    mainPaddingLength,
    headerHeight,
} from '../../utils/constants'

export const teamProfileStyle = StyleSheet.create({
    container: {
        height: windowHeight - footerHeight,
        width: windowWidth,
    },
})
