/* eslint-disable */

import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, FlatList } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import { _getNBATeamDetails } from '../../utils/api/statsAPI'

import Header from '../../components/main/iaHeader'
import TeamProfileHeader from '../../components/teamProfile/subs/header'
import TeamProfileNavBar from '../../components/teamProfile/subs/navbar'
import TeamPostsView from '../../components/teamProfile/postsView'
import TeamPlayersView from '../../components/teamProfile/playersView'

import { mainStyles } from '../../styles/main'
import { teamProfileStyle } from './style'

export default teamProfile = ({ route }) => {
    const [visible, setVisible] = useState(false)
    const [teamView, setTeamView] = useState(1) // 1 = posts, 2 = players, 3 = schedule
    //can set defaults here
    const [teamId, setTeamId] = useState(0)
    const [teamLogo, setTeamLogo] = useState('')
    const [teamBackground, setTeamBackground] = useState('')
    const [teamName, setTeamName] = useState('')

    const navigateView = (viewType) => setTeamView(viewType)

    let TeamProfileView = teamView === 1 ? TeamPostsView : TeamPlayersView

    useFocusEffect(
        useCallback(() => {
            //must always exist
            const { teamId } = route.params

            const getTeamDetails = async () => {
                const { teamDetailData } = await _getNBATeamDetails(teamId)

                setTeamId(teamDetailData.teamId)
                setTeamLogo(teamDetailData.logoUrl)
                setTeamBackground(teamDetailData.imageUrl)
                setTeamName(teamDetailData.fullName)

                setVisible(true)
            }

            getTeamDetails()

            return () => {}
        }, [])
    )

    return (
        <View style={[mainStyles.mainIAView, teamProfileStyle.container]}>
            <Header />
            {visible && (
                <>
                    <TeamProfileHeader
                        teamLogo={teamLogo}
                        teamBackground={teamBackground}
                        teamName={teamName}
                    />
                    <TeamProfileNavBar navigateView={navigateView} />
                    <TeamProfileView teamId={teamId} />
                </>
            )}
        </View>
    )
}
