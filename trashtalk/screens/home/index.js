import React, { useEffect, useState, useCallback } from 'react'
import { View, StatusBar } from 'react-native'

import HomeView from '../../mainViews/home'

import { mainStyles } from '../../styles/main'

const logoUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/logoblack.svg'

{
    /* <View contentContainerStyle={mainStyles.mainIAView}>
{MainView}
<Footer changeMainView={this.changeMainView} />
</View> */
}

//let windowHeight = Dimensions.

export default Home = () => {
    return (
        <View contentContainerStyle={mainStyles.mainIAView}>
            <HomeView />
        </View>
    )
}
