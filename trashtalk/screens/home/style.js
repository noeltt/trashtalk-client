import { StyleSheet } from 'react-native'
import { windowWidth } from '../../utils/constants'

export const homeScreenStyle = StyleSheet.create({
    container: {
        width: windowWidth,
        backgroundColor: 'yellow',

        // paddingLeft: mainPaddingLength,
        borderWidth: 1,
        borderColor: 'blue',
    },
})
