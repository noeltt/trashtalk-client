import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import { mainStyles } from '../../styles/main'

import LiveChatView from '../../mainViews/liveChat'

export default LiveChat = ({ route }) => {
    const navigation = useNavigation()
    const { params } = route
    const { view } = params
    const [viewParam, setViewParam] = useState(view)

    const updateViewParam = () => {
        setViewParam('')
        navigation.setParams({ view: '' })
    }

    useEffect(() => {
        const unsubscribe = navigation.addListener('tabPress', (e) => {
            setViewParam('default')
        })

        return unsubscribe
    }, [navigation])

    return (
        <View contentContainerStyle={mainStyles.mainIAView}>
            <LiveChatView viewParam={viewParam} updateViewParam={updateViewParam} />
        </View>
    )
}
