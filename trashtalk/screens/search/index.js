import React from 'react'
import { View } from 'react-native'

import { mainStyles } from '../../styles/main'

import SearchView from '../../mainViews/search'

export default Search = () => {
    return (
        <View contentContainerStyle={mainStyles.mainIAView}>
            <SearchView />
        </View>
    )
}
