/* eslint-disable */
import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import TeamPlayerProfileHeader from '../../components/teamPlayerProfile/subs/header'
import TeamPlayerProfileNavBar from '../../components/teamPlayerProfile/subs/navBar'
import TeamPlayerPostsView from '../../components/teamPlayerProfile/postsView'
import TeamPlayerStatsView from '../../components/teamPlayerProfile/statsView'

import { _getNBAPlayerData } from '../../utils/api/statsAPI'

import { teamPlayerProfileStyle } from './style'
import { mainStyles } from '../../styles/main'

export default teamPlayerProfile = ({ route }) => {
    const [visible, setVisible] = useState(false)
    const [playerId, setPlayerId] = useState(0)
    const [teamId, setTeamId] = useState(0)
    const [teamPlayerView, setTeamPlayerView] = useState(1)
    const [teamPlayerBackgroundUrl, setTeamPlayerBackgroundUrl] = useState('')
    const [teamPlayerHeaderName, setTeamPlayerHeaderName] = useState('')
    const [teamPlayerHeadshotUrl, setTeamPlayerHeadshotUrl] = useState('')

    const navigateView = (viewType) => setTeamPlayerView(viewType)

    let TeamPlayerProfileView = teamPlayerView === 1 ? TeamPlayerPostsView : TeamPlayerStatsView

    useEffect(() => {
        const { playerId } = route.params

        const getPlayerData = async () => {
            const { player } = await _getNBAPlayerData(playerId)
            const headerName = `${player.jerseyNumber} ${player.firstName} ${player.lastName} - ${player.positionShort}`

            setPlayerId(playerId)
            setTeamId(player.team.id)
            setTeamPlayerHeaderName(headerName)
            setTeamPlayerHeadshotUrl(player.headshotUrl)
            setTeamPlayerBackgroundUrl(player.team.backgroundImageUrl)

            setVisible(true)
        }

        getPlayerData()
    }, [])

    return (
        <View style={[mainStyles.mainIAView, teamPlayerProfileStyle.container]}>
            <Header />
            {visible && (
                <>
                    <TeamPlayerProfileHeader
                        backgroundUrl={teamPlayerBackgroundUrl}
                        headshotUrl={teamPlayerHeadshotUrl}
                        headerName={teamPlayerHeaderName}
                    />
                    <TeamPlayerProfileNavBar navigateView={navigateView} />
                    <TeamPlayerProfileView playerId={playerId} teamId={teamId} />
                </>
            )}
        </View>
    )
}
