import React, { useState, useCallback } from 'react'
import { View } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import { mainStyles } from '../../styles/main'

import ProfileView from '../../mainViews/profile'

export default Profile = ({ route }) => {
    const [isVisible, setIsVisible] = useState(false)
    const [profileUid, setProfileUid] = useState('')

    useFocusEffect(
        useCallback(() => {
            const { profileUid } = route.params ?? { profileUid: '' }
            console.log(route.params)
            setProfileUid(profileUid)
            setIsVisible(true)

            route.params = { profileUid: '' }

            return () => {
                setIsVisible(false)
            }
        }, [route])
    )

    return (
        <>
            {isVisible && (
                <View contentContainerStyle={mainStyles.mainIAView}>
                    <ProfileView profileUid={profileUid} />
                </View>
            )}
        </>
    )
}
