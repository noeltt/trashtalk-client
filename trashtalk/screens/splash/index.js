import * as React from 'react'
import { View, Image, TouchableOpacity, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { splashStyle } from './style'
import { mainStyles } from '../../styles/main'

const logoUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/logo.svg'
const logoTextUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/trash_talk.svg'

export default class SplashScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity
                style={splashStyle.splash}
                onPress={() => this.props.navigation.navigate('Login')}
            >
                <View style={mainStyles.logoWindow}>
                    <SvgUri style={mainStyles.imageFill} uri={logoUrl} />
                </View>
                <View style={mainStyles.titleWindow}>
                    <SvgUri style={mainStyles.svgForceFill} uri={logoTextUrl} />
                </View>
            </TouchableOpacity>
        )
    }
}
