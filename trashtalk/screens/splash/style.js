import React from 'react'
import { StyleSheet } from 'react-native'

export const splashStyle = StyleSheet.create({
    splash: {
        flex: 1,
        alignItems: 'center',
        //justifyContent: 'center',
        backgroundColor: '#111111',
    },
    splashBorder: {
        borderWidth: 2,
        borderColor: 'white',
    },
})
