/* eslint-disable */
import React from 'react'
import {
    View,
    Text,
    TextInput,
    Keyboard,
    TouchableWithoutFeedback,
    ScrollView,
    TouchableOpacity,
    Alert,
} from 'react-native'
import { SvgUri } from 'react-native-svg'
import { loginStyle } from './style'
import { mainStyles } from '../../styles/main'
import { userLogin } from '../../utils/apiUtil'

import '../../utils/globalManager'

const logoUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/logoblack.svg'
const logoTextUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/trash_talk_dark.svg'

export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            allowVerticalScroll: true,
            email: '',
            password: '',
        }
    }

    badAuth = () =>
        Alert.alert('Invalid Auth', 'Your username and password are not correct', [
            { text: 'OK', onPress: () => {} },
        ])

    passwordInputFocus = () => {
        // move screen down here
    }

    loginEvent = async () => {
        // set loading state

        let email = this.state.email
        let password = this.state.password

        let loginSuccess = await userLogin(email.toLowerCase(), password)

        if (loginSuccess) {
            console.log(`new api token: ${global.apiToken}`)
            console.log(global.username)
            this.props.navigation.navigate('Home')
        } else {
            this.badAuth()
        }
    }

    registrationEvent = async () => {
        this.props.navigation.navigate('Registration')
    }

    render() {
        // const { data, isLoading } = this.state;

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <ScrollView contentContainerStyle={loginStyle.loginbg}>
                    <View style={mainStyles.logoWindow}>
                        <SvgUri style={mainStyles.imageFill} uri={logoUrl} />
                    </View>
                    <View style={mainStyles.titleWindow}>
                        <SvgUri style={mainStyles.svgForceFill} uri={logoTextUrl} />
                    </View>
                    <View style={[loginStyle.inputWindow]}>
                        <Text style={loginStyle.inputLabel}>Email Address</Text>
                        <TextInput
                            style={loginStyle.inputText}
                            onChangeText={(email) => this.setState({ email })}
                        />
                    </View>
                    <View style={[loginStyle.inputWindow]}>
                        <Text style={loginStyle.inputLabel}>Password</Text>
                        <TextInput
                            secureTextEntry={true}
                            onChangeText={(password) => this.setState({ password })}
                            style={loginStyle.inputText}
                            onFocus={() => this.passwordInputFocus()}
                        />
                    </View>
                    <View style={[loginStyle.inputWindow, loginStyle.smallerWindow]}>
                        <Text style={loginStyle.forgotPasswordText}>Forgot Password?</Text>
                    </View>
                    <TouchableOpacity
                        style={loginStyle.loginButton}
                        onPress={() => this.loginEvent()}
                    >
                        <Text style={loginStyle.loginButtonText}>Login</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.registrationEvent()}>
                        <Text style={loginStyle.createAccountText}>Create Account</Text>
                    </TouchableOpacity>
                </ScrollView>
            </TouchableWithoutFeedback>
        )
    }
}
