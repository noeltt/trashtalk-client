import { StyleSheet } from 'react-native'

export const loginStyle = StyleSheet.create({
    loginbg: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white',
    },
    loginBorder: {
        borderWidth: 2,
        borderColor: 'black',
    },
    loginText: {
        height: '100%',
    },
    inputWindow: {
        marginTop: 30,
        width: 335,
        height: 43,
    },
    smallerWindow: {
        marginTop: 5,
    },
    inputLabel: {
        marginLeft: 4,
        lineHeight: 14,
        fontSize: 12,
        // fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '500',
        color: '#8F8F8F',
    },
    inputText: {
        height: 29,
        width: '100%',
        paddingBottom: 0,
        fontSize: 16,
        lineHeight: 19,
        fontStyle: 'normal',
        fontWeight: 'normal',
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1,
        letterSpacing: 0.3,
    },
    rememberMeView: {
        height: '80%',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'blue',
    },
    forgotPasswordText: {
        alignSelf: 'flex-end',
        color: 'black',
        fontWeight: '600',
        fontStyle: 'normal',
        fontSize: 12,
    },
    loginButton: {
        marginTop: 40,
        width: 335,
        height: 48,
        backgroundColor: '#111111',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    loginButtonText: {
        // // fontFamily: 'Calibri',
        color: 'white',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
    },
    createAccountText: {
        marginTop: 15,
        // //fontFamily: 'Calibri',
        color: 'black',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 18,
    },
})
