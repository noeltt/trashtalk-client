import React from 'react'
import { View, Text } from 'react-native'
import RegistrationView from '../../components/registration'

import { mainStyles } from '../../styles/main'
import { registrationStyle } from './style'

export default Registration = () => {
    return (
        <View contentContainerStyle={[mainStyles.mainIAView]}>
            <RegistrationView />
        </View>
    )
}
