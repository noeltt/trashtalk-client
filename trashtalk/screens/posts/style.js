import { StyleSheet } from 'react-native'
import { windowHeight, footerHeight, textInputHeight, headerHeight } from '../../utils/constants'

export const postMainStyle = StyleSheet.create({
    container: {
        height: windowHeight - footerHeight,
        // borderWidth: 2,
        // borderColor: 'black'
    },
    viewContainer: {
        height: windowHeight - headerHeight - footerHeight - textInputHeight,
        backgroundColor: 'white',
    },
    commentViewContainer: {
        height: windowHeight - headerHeight - footerHeight - textInputHeight,
        // borderWidth: .5
    },
})
