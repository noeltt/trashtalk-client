/* eslint-disable */
import React, { useState, useCallback } from 'react'
import { View, FlatList, Keyboard, KeyboardAvoidingView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import { _getPostDetails, _getPostComments } from '../../utils/api/postAPI'
import { getTimeText } from '../../utils/utils'

import Header from '../../components/main/iaHeader'
import IATextInput from '../../components/main/subs/textInput'
import PostHeader from '../../components/posts/postHeader'
import PostDetails from '../../components/posts/postDetails'
import SubHeaderOne from '../../components/main/subs/headerOne'
import SubHeaderOneButton from '../../components/main/subs/headerOneButton'
import CommentCard from '../../components/posts/commentCard'

import { mainStyles } from '../../styles/main'
import { postMainStyle } from './style'

const subHeaderTitle = 'Latest'

export default Posts = ({ route }) => {
    const [isVisible, setIsVisible] = useState(false)
    const [postHeaderData, setPostHeaderData] = useState({})
    const [postBodyData, setPostBodyData] = useState({})
    const [commentsData, setCommentsData] = useState([])
    const [detailsLoaded, setDetailsLoaded] = useState(false)
    const [commentsLoaded, setCommentsLoaded] = useState(false)

    const refreshComments = async (postId) => {
        // setCommentsLoaded(false)
        Keyboard.dismiss()
        setCommentsData(false)

        const data = await _getPostComments(postId)
        let { comments } = data

        let commentDataPre = comments.map(({ authorData, commentData, userData }) => ({
            id: commentData.id,
            authorImageUrl: authorData.avatarUrl,
            authorUsername: authorData.username,
            message: commentData.message,
            reactCount: commentData.likeCount + commentData.trashCount,
            timeText: getTimeText(commentData.minDiff),
            userLike: userData.userLike,
            userTrash: userData.userTrash,
        }))

        setCommentsData(commentDataPre)
        setCommentsLoaded(true)
    }

    useFocusEffect(
        useCallback(() => {
            // console.log(route.params)
            if (route.params?.postId) {
                let { postId } = route.params

                const getPostDetailsData = async () => {
                    const data = await _getPostDetails(postId)
                    let { authorData, postData, userData } = data

                    let retPostHeaderData = {
                        postId,
                        authorImageUrl: authorData.avatarUrl,
                        authorUsername: authorData.username,
                        isAdmin: authorData.isAdmin,
                        postType: postData.type,
                        timeText: getTimeText(postData.minDiff),
                    }

                    let retPostBodyData = {
                        postId,
                        postTitle: postData.title,
                        trashCount: postData.trashCount,
                        likeCount: postData.likeCount,
                        commentCount: postData.commentCount,
                        postType: postData.type,
                        pollOptions: postData.pollOptions,
                        didUserVote: postData.didUserVote,
                        userVoteOption: postData.userVoteOption,
                        totalVoteCount: postData.totalVoteCount,
                        userLike: userData.userLike,
                        userTrash: userData.userTrash,
                    }

                    setPostHeaderData({ ...retPostHeaderData })
                    setPostBodyData({ ...retPostBodyData })
                    setDetailsLoaded(true)
                }

                const getPostCommentsData = async () => {
                    const data = await _getPostComments(postId)
                    let { comments } = data

                    let commentDataPre = comments.map(({ authorData, commentData, userData }) => ({
                        id: commentData.id,
                        authorImageUrl: authorData.avatarUrl,
                        authorUsername: authorData.username,
                        message: commentData.message,
                        likeCount: commentData.likeCount,
                        trashCount: commentData.trashCount,
                        timeText: getTimeText(commentData.minDiff),
                        userLike: userData.userLike,
                        userTrash: userData.userTrash,
                    }))

                    setCommentsData(commentDataPre)

                    setCommentsLoaded(true)
                }

                getPostDetailsData()
                // need to be better about this
                setIsVisible(true)
                getPostCommentsData()
            } else {
                // some client error handling
                // alert, this post has been deleted or could not be found
            }

            return () => {
                setIsVisible(false)
                setDetailsLoaded(false)
                setCommentsLoaded(false)
            }
        }, [])
    )

    const renderItem = ({ item }) => <CommentCard commentData={item} />

    return (
        <>
            {isVisible && (
                <KeyboardAvoidingView
                    behavior={'height'}
                    keyboardVerticalOffset={45}
                    style={postMainStyle.container}
                >
                    <Header />
                    <View style={postMainStyle.viewContainer}>
                        {detailsLoaded && <PostHeader postHeaderData={postHeaderData} />}
                        {detailsLoaded && <PostDetails postBodyData={postBodyData} />}
                        <SubHeaderOne
                            title={subHeaderTitle}
                            style={[mainStyles.topBorderLine, mainStyles.paddingLeftDefault]}
                        >
                            <SubHeaderOneButton />
                        </SubHeaderOne>
                        {commentsLoaded && (
                            <FlatList
                                data={commentsData}
                                renderItem={renderItem}
                                keyExtractor={(item, index) => item.id}
                                style={mainStyles.paddingLeftDefault}
                            />
                        )}
                    </View>
                    {detailsLoaded && commentsLoaded && (
                        <IATextInput
                            postId={postBodyData.postId}
                            refreshComments={refreshComments}
                        />
                    )}
                </KeyboardAvoidingView>
            )}
        </>
    )
}
