import { StyleSheet } from 'react-native'
import { windowHeight, windowWidth, footerHeight } from '../../../utils/constants'

export const gameRoomStyle = StyleSheet.create({
    container: {
        minHeight: windowHeight - footerHeight,
        width: windowWidth,
        // paddingLeft: mainPaddingLength
    },
})
