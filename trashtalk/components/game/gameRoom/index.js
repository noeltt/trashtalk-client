/* eslint-disable */
import React, { useState, useCallback, useEffect, useRef } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { View, Keyboard, KeyboardAvoidingView } from 'react-native'

import GameRoomScoreBoard from '../subs/gameRoomScoreBoard'
import GameRoomNavBar from '../subs/gameRoomNav'
import ChatRoom from '../chatRoom'
import ProfileView from '../profileView'

import ChatSocket from '../../../utils/chat'
import { getLocalTimeText } from '../../../utils/utils'
import { _getGameData } from '../../../utils/api/statsAPI'
import { _getGameChatMessages } from '../../../utils/api/chatAPI'

import { mainStyles } from '../../../styles/main'
import { gameRoomStyle } from './style'

const chatMaxLength = 100

const sendChatMessage = (data) => {
    // console.log({data})
    ChatSocket.postChat(data)
}

let initialGameData = {
    homeScore: 0,
    awayScore: 0,
    timeText: '00:00',
    quarter: 1,
}

let initialGameTeamData = {
    home: {
        logoUrl: '',
        shortName: '',
        record: '',
    },
    away: {
        logoUrl: '',
        shortName: '',
        record: '',
    },
}

export default GameRoom = ({ gameId }) => {
    const [currentGameView, setGameView] = useState('chat')
    const [liveGameData, setLiveGameData] = useState(initialGameData)
    const [liveGameTeamData, setLiveGameTeamData] = useState(initialGameTeamData)
    const [chatMessages, setChatMessages] = useState([])
    const [emojiSpamArray, setEmojiSpamArray] = useState([])
    const [testGameSocket, setGameSocket] = useState(null)
    const [someoneTyping, setSomeoneTyping] = useState(false)

    const chatRef = useRef({})
    const timerRef = useRef(null)
    const typingTimerRef = useRef(null)

    chatRef.current = chatMessages

    useEffect(() => {
        const loadGameChats = async () => {
            const { existingChatMessages = [] } = await _getGameChatMessages(gameId)

            if (existingChatMessages.length > 0) {
                existingChatMessages.map((x) => {
                    x.timeText = getLocalTimeText(x.createdAt)
                })

                setChatMessages([...existingChatMessages])
            }
        }

        const loadGameData = async () => {
            const { game } = await _getGameData(gameId)
            const preLiveGameData = {
                homeScore: game.homeTeam.score,
                awayScore: game.awayTeam.score,
                timeText: game.gameClock,
                quarter: game.quarter,
            }

            const preLiveGameTeamData = {
                home: {
                    logoUrl: game.homeTeam.logoUrl,
                    shortName: game.homeTeam.teamShortName,
                    record: game.homeTeam.teamRecord,
                },
                away: {
                    logoUrl: game.awayTeam.logoUrl,
                    shortName: game.awayTeam.teamShortName,
                    record: game.awayTeam.teamRecord,
                },
            }

            setLiveGameData(preLiveGameData)
            setLiveGameTeamData(preLiveGameTeamData)

            //even timeout here doesn't matter
            await loadGameChats()
        }

        loadGameData()
        loadGameChats()
        return () => {
            clearTimeout(timerRef.current)
            clearTimeout(typingTimerRef.current)
        }
    }, [])



    const slide = (emojiType) => {
        let keyInt = 1
        keyInt = Math.floor(Math.random() * 10000) + 1
        let tempArray = [...emojiSpamArray] // , {key: keyInt, emojiType }]
        tempArray.push({ key: keyInt, emojiType })
        setEmojiSpamArray((x) => [...x.slice(x.length - 20), { key: keyInt, emojiType }])

        clearTimeout(timerRef.current)
        timerRef.current = setTimeout(() => {
            setEmojiSpamArray((x) => [])
        }, 5000)
    }

    // socket events
    // connecting and disconnecting from socket
    useFocusEffect(
        useCallback(() => {
            // connect to socket room
            isActive = true
            const gameSocket = ChatSocket.initialize()
            setGameSocket(gameSocket)
            console.log('socket connect: ' + gameId)

            ChatSocket.joinRoom(gameId)

            gameSocket.on('chat_posted', (data) => {
                let chatMessagesHolder = chatRef.current
                chatMessagesHolder.unshift(data)

                if (chatMessagesHolder.length > chatMaxLength) chatMessagesHolder.pop()
                setChatMessages([...chatMessagesHolder])
            })

            // get current game scores
            gameSocket.on('game_stats_update', (data) => {
                setLiveGameData(data)
            })

            gameSocket.on('someone_typing', (data) => {
                const { username } = data
                if (username != global.username) {
                    setSomeoneTyping(true)
                    clearTimeout(typingTimerRef.current)
                    typingTimerRef.current = setTimeout(() => {
                        setSomeoneTyping(false)
                    }, 2000)
                }
            })

            gameSocket.on('emoji_posted', (data) => {
                const { emojiType } = data

                slide(emojiType)
            })

            return async () => {
                ChatSocket.socketDisconnect(gameId)
            }
        }, [])
    )

    const changeGameView = (gameView) => setGameView(gameView)

    const GameView = currentGameView === 'chat' ? ChatRoom : ProfileView
    // game stats view to come

    return (
        <View style={[mainStyles.bgWhite, gameRoomStyle.container]}>
            <GameRoomScoreBoard liveGameTeamData={liveGameTeamData} liveGameData={liveGameData} />
            <GameRoomNavBar gameId={gameId} gameView={currentGameView} changeGameView={changeGameView} />
            <GameView
                sendChatMessage={sendChatMessage}
                testGameSocket={testGameSocket}
                emojiSpamArray={emojiSpamArray}
                chatMessages={chatRef.current}
                someoneTyping={someoneTyping}
                gameId={gameId}
            />
        </View>
    )
}
