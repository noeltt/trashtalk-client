import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    mainPaddingLength,
    gameRoomHeaderHeight,
    headerHeight,
} from '../../../utils/constants'

const chatRoomHeight = windowHeight - (gameRoomHeaderHeight + footerHeight + headerHeight)

export const gameProfileStyle = StyleSheet.create({
    container: {
        height: chatRoomHeight,
        width: windowWidth,
        paddingLeft: mainPaddingLength,
    },
    postsContainer: {
        height: chatRoomHeight - headerHeight,
        width: windowWidth - mainPaddingLength,

        // borderWidth: .5,
        // borderColor: 'black',
        // backgroundColor: 'lightgray'
    },
})
