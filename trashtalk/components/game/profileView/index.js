/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, FlatList } from 'react-native'

import SubHeaderOne from '../../main/subs/headerOne'
import SubHeaderOneButton from '../../main/subs/headerOneButton'
import PostCard from '../../main/subs/postCard'
import CreatePostButton from '../../posts/subs/createPostButton'

import { mainStyles } from '../../../styles/main'
import { gameProfileStyle } from './style'

import { _getRecentPosts } from '../../../utils/api/postAPI'

const subHeaderTitle = 'Latest'

export default ProfileView = () => {
    const [posts, setPosts] = useState([])

    const renderItem = ({ item }) => <PostCard postData={item} />

    useEffect(() => {
        const getPosts = async () => {
            const ret = await _getRecentPosts()
            if (ret) {
                const { posts } = ret
                setPosts(posts)
            } else {
                console.log('client error getting recent posts')
            }
        }

        getPosts()
    }, [])

    return (
        <View style={[mainStyles.bgWhite, gameProfileStyle.container]}>
            <SubHeaderOne title={subHeaderTitle}>
                <SubHeaderOneButton />
            </SubHeaderOne>
            <FlatList
                data={posts}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
                style={gameProfileStyle.postsContainer}
            />
            <CreatePostButton />
        </View>
    )
}
