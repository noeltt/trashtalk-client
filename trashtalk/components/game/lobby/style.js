import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    mainPaddingLength,
    headerHeight,
} from '../../../utils/constants'

export const lobbyStyle = StyleSheet.create({
    container: {
        height: windowHeight - headerHeight - footerHeight,
        width: windowWidth,
        paddingLeft: mainPaddingLength,
    },
    lobbyListContainer: {
        flex: 1,
        width: windowWidth,
        marginTop: mainPaddingLength,
    },
})
