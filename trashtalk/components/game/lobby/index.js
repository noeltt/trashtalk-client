import React, { useEffect, useState } from 'react'
import { View, FlatList } from 'react-native'
import { useIsFocused } from '@react-navigation/native'

import SearchHeader from '../../main/searchHeader'
/* eslint-disable */
import SubHeaderOne from '../../main/subs/headerOne'
import SubHeaderOneButton from '../../main/subs/headerOneButton'
import LobbyRoomCard from '../subs/roomCard'

import { _getUpcomingGames } from '../../../utils/api/statsAPI'

import { mainStyles } from '../../../styles/main'
import { lobbyStyle } from './style'

const subHeaderTitle = 'Today'

export default Lobby = ({ gotoChatroom }) => {
    const isFocused = useIsFocused()
    const [upcomingGames, setUpcomingGames] = useState([])

    useEffect(() => {
        const getGames = async () => {
            const { games } = await _getUpcomingGames()
            setUpcomingGames(games)
        }

        if (isFocused) {
            getGames()
        }
    }, [isFocused])

    const renderItem = ({ item }) => (
        <LobbyRoomCard
            key={item.key}
            game={item}
            gameId={item.gameId}
            gotoChatroom={gotoChatroom}
        />
    )

    return (
        <View style={lobbyStyle.container}>
            {/* <SearchHeader /> */}
            <SubHeaderOne title={subHeaderTitle} style={mainStyles.topBorderLine}>
                <SubHeaderOneButton />
            </SubHeaderOne>
            <FlatList
                data={upcomingGames}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
                style={lobbyStyle.lobbyListContainer}
            />
        </View>
    )
}
