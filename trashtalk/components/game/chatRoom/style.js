/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    mainPaddingLength,
    gameRoomHeaderHeight,
    headerHeight,
} from '../../../utils/constants'

const messageInputHeight = 40
const chatRoomHeight =
    windowHeight - (gameRoomHeaderHeight + footerHeight + headerHeight) - messageInputHeight
const messageWindowHeight = chatRoomHeight - messageInputHeight

export const chatroomStyle = StyleSheet.create({
    container: {
        //height: chatRoomHeight,
        display: 'flex',
        width: windowWidth,
        flexDirection: 'column',
    },
    someoneTypingContainer: {
        position: 'absolute',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        bottom: messageInputHeight,
        color: 'lightgray',
    },
    messageInputContainer: {
        position: 'absolute',
        width: windowWidth,
        height: messageInputHeight,
        paddingLeft: mainPaddingLength,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        borderColor: 'lightgray',
        flexDirection: 'row',
        alignItems: 'center',
        bottom: 0,

        // backgroundColor: 'powderblue'
    },
    emojiInputContainer: {
        width: windowWidth,
        height: messageInputHeight,
        paddingLeft: mainPaddingLength,
        // borderTopWidth: .5,
        // borderBottomWidth: .5,
        // borderColor: 'blue',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

        // position: 'absolute',
        // bottom: 0,

        backgroundColor: '#F2F2F2',
    },
    emojiTouchable: {
        height: '80%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    emojiImage: {
        minHeight: '60%',
        aspectRatio: 1,
    },
    messageInput: {
        height: '100%',
        width: windowWidth - (mainPaddingLength + messageInputHeight),
        // //fontFamily: 'SFPro Display',
        fontWeight: '500',
        fontSize: 16,
        lineHeight: 19,

    },
    modalLinkButtonStyle: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 20,
        top: 10,
        right: 10,
        width: 100,
        height: 20,
        borderRadius: 15,
        backgroundColor: 'powderblue'
    },
    messageInputButtonContainer: {
        height: '100%',
        maxHeight: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',

        // backgroundColor: 'gray'
    },
    messageInputButton: {
        marginTop: 10,
        minWidth: '50%',
        aspectRatio: 1,
    },
    messagesContainer: {
        width: windowWidth,

        paddingLeft: mainPaddingLength,
    },
})
