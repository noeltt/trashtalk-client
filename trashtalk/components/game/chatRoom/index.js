/* eslint-disable */
import React, { useState, useCallback, useEffect, useRef } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { useDebounce } from 'use-debounce'
import {
    Animated,
    View,
    TouchableOpacity,
    TextInput,
    Text,
    FlatList,
    Keyboard,
    KeyboardAvoidingView,
} from 'react-native'

import { SvgUri } from 'react-native-svg'

import ChatSocket from '../../../utils/chat'

import {
    windowHeight,
    windowWidth,
    footerHeight,
    mainPaddingLength,
    gameRoomHeaderHeight,
    headerHeight,
} from '../../../utils/constants'

import { _getGameLinks } from '../../../utils/api/statsAPI'
import GameLinksModal from '../subs/linkModal'

const messageInputHeight = 40
const chatRoomHeight =
    windowHeight - (gameRoomHeaderHeight + footerHeight + headerHeight) - messageInputHeight
const messageWindowHeight = chatRoomHeight - messageInputHeight

// import io from 'socket.io-client'

import '../../../utils/globalManager'
import { inputText, fireLogo, blueTrash, redHeart, testTeam } from '../../../utils/svgs'
// chatroommessage card
import MessageCard from '../subs/messageCard'
import EmojiSpam from '../subs/emojiSpam'

import { mainStyles } from '../../../styles/main'
import { chatroomStyle } from './style'

const messagePlaceholder = 'Talk your sh*t...'

export default ChatRoom = ({
    sendChatMessage,
    gameSocket,
    chatMessages,
    emojiSpamArray,
    someoneTyping,
    gameId,
}) => {
    const [message, setMessage] = useState('')
    const [keyboardShowing, setKeyboardShowing] = useState(false)
    const [keyboardHeight, setKeyboardHeight] = useState(0)
    const [isTyping, setIsTyping] = useState(false)
    const [modalVisible, setModalVisible] = useState(false)
    const [gameLinks, setGameLinks] = useState([])

    // keeps track of setting is typing
    const [debMessage] = useDebounce(message, 1000)

    const hideModal = () => {
        setModalVisible(false)
    }

    useEffect(() => {
        setIsTyping(false)
    }, [debMessage])

    const chatRoomKeyboardHeight =
        chatRoomHeight - (keyboardHeight - (messageInputHeight * 2 + footerHeight - 10))
    const messageWindowKeyboardHeight =
        messageWindowHeight - (keyboardHeight - (messageInputHeight * 2 + footerHeight - 5))

    const sendMessage = () => {
        let data = {
            message: message,
            username: global.username,
            gameId: gameId,
        }

        if (message.length > 0) {
            sendChatMessage(data)
            setMessage('')
        }

        // Keyboard.dismiss()
    }

    const textChange = (message) => {
        setMessage(message)
        if (!isTyping) {
            setIsTyping(true)
            ChatSocket.typing({ username: global.username, gameId })
        }
    }

    const sendEmoji = (emojiType) => {
        const data = {
            emojiType,
            gameId,
        }

        ChatSocket.spamEmoji(data)
        Keyboard.dismiss()
    }

    const renderItem = ({ item }) => <MessageCard messageData={item} />

    useEffect(() => {

        const getGameLinks = async () => {
            const { links } = await _getGameLinks(gameId)
            if (links && links.length > 0) {
                console.log({ links })
                setGameLinks([...links])
            }
        }

        getGameLinks()


        const keyboardShowingListener = Keyboard.addListener('keyboardDidShow', (e) => {
            const tempKeyboardHeight = e.endCoordinates.height
            setKeyboardHeight(tempKeyboardHeight)
            setKeyboardShowing(true)
        })
        const keyboardHidingListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardHeight(0)
            setKeyboardShowing(false)
        })
        return () => {
            keyboardShowingListener.remove()
            keyboardHidingListener.remove()
        }
    }, [])

    return (
        <>
            <GameLinksModal gameLinks={gameLinks} modalVisible={modalVisible} hideModal={hideModal} />
            <View>
                <View
                    style={[
                        mainStyles.bgWhite,
                        chatroomStyle.container,
                        { height: keyboardShowing ? chatRoomKeyboardHeight : chatRoomHeight },
                    ]}
                >
                    <FlatList
                        data={chatMessages}
                        renderItem={renderItem}
                        keyboardDismissMode={'interactive'}
                        style={[
                            chatroomStyle.messagesContainer,
                            {
                                maxHeight: keyboardShowing
                                    ? messageWindowKeyboardHeight
                                    : messageWindowHeight,
                                marginBottom: someoneTyping ? 60 : 0
                            },
                        ]}
                        keyExtractor={(item, index) => index}
                        inverted={true}
                    />
                    {gameLinks.length > 0 && <TouchableOpacity onPress={() => { setModalVisible(!modalVisible) }} style={chatroomStyle.modalLinkButtonStyle}><Text style={{ color: 'white' }}>Game Links</Text></TouchableOpacity>}
                    {someoneTyping && <View style={chatroomStyle.someoneTypingContainer}><Text style={{ color: 'gray' }}>Someone is typing...</Text></View>}

                    <View style={chatroomStyle.messageInputContainer}>
                        <TextInput
                            placeholder={messagePlaceholder}
                            onChangeText={(message) => textChange(message)}
                            onSubmitEditing={sendMessage}
                            value={message}
                            style={chatroomStyle.messageInput}
                        />
                        <TouchableOpacity
                            style={[chatroomStyle.messageInputButtonContainer]}
                            onPress={sendMessage}
                        >
                            <SvgUri style={chatroomStyle.messageInputButton} uri={inputText} />
                        </TouchableOpacity>
                    </View>
                </View>
                {!keyboardShowing && (
                    <View style={chatroomStyle.emojiInputContainer}>
                        <TouchableOpacity
                            style={chatroomStyle.emojiTouchable}
                            onPress={() => sendEmoji('heart')}
                        >
                            <SvgUri style={chatroomStyle.emojiImage} uri={redHeart} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={chatroomStyle.emojiTouchable}
                            onPress={() => sendEmoji('fire')}
                        >
                            <SvgUri style={chatroomStyle.emojiImage} uri={fireLogo} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={chatroomStyle.emojiTouchable}
                            onPress={() => sendEmoji('trash')}
                        >
                            <SvgUri style={chatroomStyle.emojiImage} uri={blueTrash} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={chatroomStyle.emojiTouchable}
                            onPress={() => sendEmoji('team')}
                        >
                            <SvgUri style={chatroomStyle.emojiImage} uri={testTeam} />
                        </TouchableOpacity>
                    </View>
                )}
                {emojiSpamArray.map((x, index) => (
                    <EmojiSpam key={x.key} x={x} index={index} />
                ))}
            </View>
        </>
    )
}
