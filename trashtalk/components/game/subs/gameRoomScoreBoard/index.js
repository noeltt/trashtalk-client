import React from 'react'
import { View, Text } from 'react-native'
import FastImage from 'react-native-fast-image'

import { getQuarterText } from '../../../../utils/utils'

import { scoreboardStyle } from './style'

const ScoreCard = ({ teamData, score }) => (
    <View style={scoreboardStyle.scorecardContainer}>
        <View style={scoreboardStyle.scoreContainer}>
            <Text style={scoreboardStyle.scoreText}>{score}</Text>
        </View>
        <View style={scoreboardStyle.teamContainer}>
            <FastImage
                style={scoreboardStyle.teamLogo}
                source={{
                    uri: teamData.logoUrl,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={scoreboardStyle.teamNameText}>{teamData.shortName}</Text>
            <Text style={scoreboardStyle.teamRecordText}>{teamData.record}</Text>
        </View>
    </View>
)

export default GameRoomScoreBoard = ({ liveGameTeamData, liveGameData }) => {
    let { homeScore, awayScore, timeText, quarter } = liveGameData

    return (
        <View style={scoreboardStyle.scoreboardContainer}>
            <ScoreCard teamData={liveGameTeamData.home} score={homeScore} />
            <View style={scoreboardStyle.gameclockContainer}>
                <Text style={scoreboardStyle.gameQuarterText}>{getQuarterText(quarter)}</Text>
                <Text style={scoreboardStyle.gameTimeText}>{timeText}</Text>
            </View>
            <ScoreCard teamData={liveGameTeamData.away} score={awayScore} />
        </View>
    )
}
