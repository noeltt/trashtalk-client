import { StyleSheet } from 'react-native'
import {
    windowWidth,
    mainPaddingLength,
    scoreboardBottomGap,
    scoreboardHeight,
} from '../../../../utils/constants'

export const scoreboardStyle = StyleSheet.create({
    scoreboardContainer: {
        height: scoreboardHeight,
        width: windowWidth,
        flexDirection: 'row',
        marginBottom: scoreboardBottomGap,
    },
    scorecardContainer: {
        height: '100%',
        width: '40%',
        // backgroundColor: 'powderblue'
    },
    scoreContainer: {
        height: '60%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scoreText: {
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '700',
        fontSize: 30,
    },
    teamContainer: {
        height: '40%',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: mainPaddingLength,
        // backgroundColor: 'yellow'
    },
    teamLogo: {
        height: '90%',
        aspectRatio: 1,
    },
    teamNameText: {
        marginLeft: 10,
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '700',
        fontSize: 18,
    },
    teamRecordText: {
        marginLeft: 5,
        // fontFamily: "//SF Pro Display",
        fontWeight: '600',
        fontSize: 12,
    },
    gameclockContainer: {
        height: '100%',
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'skyblue'
    },
    gameTimeText: {
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '500',
        fontSize: 12,
    },
    gameQuarterContainer: {
        // marginBottom: 4
    },
    gameQuarterText: {
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '700',
        fontSize: 16,
    },
})
