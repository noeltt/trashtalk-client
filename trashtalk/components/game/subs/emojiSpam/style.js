import { StyleSheet } from 'react-native'
import { windowWidth, navBarHeight } from '../../../../utils/constants'

export const emojiSpamStyle = StyleSheet.create({
    emojiSpamSeed: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 100,
        right: 60,
        maxHeight: 50,
        minHeight: 50,
        maxWidth: 50,
        minWidth: 50,
    },
    emojiSpamImage: {
        minHeight: '60%',
        aspectRatio: 1,
    },
})
