import React, { useState, useEffect, useRef, memo } from 'react'
import { Animated, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { emojiSpamStyle } from './style'
import { fireLogo, redHeart, testTeam, blueTrash } from '../../../../utils/svgs'

export default emojiSpam = ({ x, index }) => {
    const testAnim = useRef(new Animated.Value(0)).current
    const testAnim2 = useRef(new Animated.Value(0)).current
    const testAnim3 = useRef(new Animated.ValueXY()).current

    const fadeAnim = useRef(new Animated.Value(1)).current

    const emojiLogo =
        x.emojiType === 'trash'
            ? blueTrash
            : x.emojiType === 'heart'
            ? redHeart
            : x.emojiType === 'team'
            ? testTeam
            : fireLogo

    useEffect(() => {
        Animated.stagger(0, [
            Animated.timing(testAnim, {
                toValue: 1,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 2,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 3,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 4,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 5,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 6,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 7,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 8,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(testAnim, {
                toValue: 9,
                duration: 600,
                useNativeDriver: true,
            }),
            Animated.timing(fadeAnim, {
                toValue: 0,
                duration: 750,
                useNativeDriver: true,
            }),
        ]).start(({ finished }) => {
            // removeEmojiFromArray(index)
        })
    }, [])

    const vertical = testAnim.interpolate({
        inputRange: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        outputRange: [0, -30, -40, -60, -70, -90, -100, -120, -130, -150],
    })

    const horizontal = testAnim.interpolate({
        inputRange: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        outputRange: [0, -17, -17, 17, 17, -17, -17, 17, 17, 0],
    })

    const animStyle = {
        transform: [
            {
                translateY: vertical,
            },
            {
                translateX: horizontal,
            },
        ],
    }

    return (
        <Animated.View
            key={x.key}
            style={[emojiSpamStyle.emojiSpamSeed, animStyle, { opacity: fadeAnim }]}
        >
            <SvgUri style={emojiSpamStyle.emojiSpamImage} uri={emojiLogo} />
        </Animated.View>
    )
}
