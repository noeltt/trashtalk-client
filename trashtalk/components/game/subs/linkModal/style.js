/* eslint-disable */
import { StyleSheet } from 'react-native'
import { mainPaddingLength, insetTop, windowWidth, windowHeight } from '../../../utils/constants'

const gameLinksModalStyle = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        position: 'absolute',
        bottom: 0,
        height: 300,
        width: '100%',
        borderWidth: 1,
        borderColor: 'black',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: 'white',
    },
    gameLinkButton: {
        display: 'flex',
        paddingHorizontal: 10,
        paddingVertical: 5,
        marginVertical: 20,
        backgroundColor: 'black',
        borderRadius: 20,
    }
})

export default gameLinksModalStyle