/*eslint-disable*/
import React, { useEffect } from "react"
import { View, Modal, Text, Linking, TouchableOpacity } from "react-native"
import gameLinksModalStyle from "./style"

const GameLinksModal = ({ gameLinks, modalVisible, hideModal }) => {

    //TODO: check usable links
    useEffect(() => {
        console.log(gameLinks)
    }, [])

    // TODO: create swipe wrapper
    return (<Modal animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => { }}>
        <View onTouchStart={e => this.touchY = e.nativeEvent.pageY}
            onTouchEnd={e => {
                if (this.touchY - e.nativeEvent.pageY < -20) {
                    hideModal()
                }
            }} onPress={() => hideModal()} style={gameLinksModalStyle.container}>
            {gameLinks.map((link, i) => <TouchableOpacity onPress={() => Linking.openURL(link)} style={gameLinksModalStyle.gameLinkButton} key={i}><Text style={{ color: 'white' }}>Game Link {i + 1}</Text></TouchableOpacity>)}
        </View>
    </Modal>)
}

export default GameLinksModal