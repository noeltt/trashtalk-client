/* eslint-disable */
import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { gameNavStyle } from './style'

const GameRoomNavBar = ({ gameId, changeGameView, gameView }) => {

    return (
        <View style={gameNavStyle.container}>
            <TouchableOpacity
                style={[gameNavStyle.navButtonContainer, { borderBottomWidth: gameView === 'profile' ? 3 : 1, borderBottomColor: gameView === 'profile' ? 'gold' : 'lightgray' }]}
                onPress={() => changeGameView('profile')}
            >
                <Text style={[gameNavStyle.navText, { fontWeight: gameView === 'profile' ? 'bold' : 'normal' }]}>Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={[gameNavStyle.navButtonContainer, { borderBottomWidth: gameView === 'chat' ? 3 : 1, borderBottomColor: gameView === 'chat' ? 'gold' : 'lightgray' }]}
                onPress={() => changeGameView('chat')}
            >
                <Text style={[gameNavStyle.navText, { fontWeight: gameView === 'chat' ? 'bold' : 'normal' }]}>Live Chat</Text>
            </TouchableOpacity>
            <TouchableOpacity style={gameNavStyle.navButtonContainer}>
                <Text style={[gameNavStyle.navText, { color: 'lightgray' }]}>Stats</Text>
            </TouchableOpacity>
        </View>
    )
}

export default GameRoomNavBar