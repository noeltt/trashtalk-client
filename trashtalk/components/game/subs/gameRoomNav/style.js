import { StyleSheet } from 'react-native'
import { windowWidth, navBarHeight } from '../../../../utils/constants'

export const gameNavStyle = StyleSheet.create({
    container: {
        height: navBarHeight,
        width: windowWidth,
        flexDirection: 'row',
    },
    navButtonContainer: {
        height: '100%',
        width: windowWidth / 3,
        justifyContent: 'center',
        alignItems: 'center',

        borderBottomWidth: 1,
        borderColor: 'lightgray',
    },
    navButtonContainerHalf: {
        height: '100%',
        width: windowWidth / 2,
        justifyContent: 'center',
        alignItems: 'center',

        borderBottomWidth: 1,
        borderColor: 'lightgray',
    },
    navText: {
        color: 'black',
        // fontFamily: 'LEMON MILK Pro FTR',
        fontWeight: '400',
        fontSize: 17,
    },
})
