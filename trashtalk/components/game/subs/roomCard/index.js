import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'

import { getQuarterText } from '../../../../utils/utils'

import { mainStyles } from '../../../../styles/main'
import { roomCardStyle } from './style'

const TeamSlot = ({ teamData }) => (
    <View style={roomCardStyle.teamSlot}>
        <FastImage
            style={roomCardStyle.teamLogo}
            source={{
                uri: teamData.logoUrl,
                priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.contain}
        />

        <Text style={roomCardStyle.teamName}>{teamData.teamName}</Text>
        <Text style={roomCardStyle.teamRecord}>({teamData.teamRecord})</Text>
        <Text style={roomCardStyle.teamScore}>{teamData.score}</Text>
    </View>
)

export default LobbyRoomCard = ({ game, gameId, gotoChatroom }) => {
    const navigateToChatroom = () => {
        gotoChatroom(gameId)
    }

    let { homeTeam, awayTeam } = game

    return (
        <TouchableOpacity
            style={[roomCardStyle.container, mainStyles.bgLightGray]}
            onPress={navigateToChatroom}
        >
            <View style={roomCardStyle.topContainer}>
                <TeamSlot teamData={homeTeam} />
                <TeamSlot teamData={awayTeam} />
            </View>
            <View style={roomCardStyle.bottomContainer}>
                <Text style={roomCardStyle.gameTimeText}>
                    {getQuarterText(game.quarter)} - {game.gameClock}
                </Text>
                <Text style={roomCardStyle.participants}>
                    People in Room: {game.participantCount}
                </Text>
            </View>
        </TouchableOpacity>
    )
}
