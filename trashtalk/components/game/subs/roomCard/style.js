import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength } from '../../../../utils/constants'

const containerHeight = 120
const bottomContainerHeight = 30
const topContainerHeight = containerHeight - bottomContainerHeight

export const roomCardStyle = StyleSheet.create({
    container: {
        height: 120,
        width: windowWidth - 2 * mainPaddingLength,
        marginBottom: mainPaddingLength,
        borderRadius: 5,
        // borderWidth: .5,
        // borderColor: 'black'
    },
    topContainer: {
        height: topContainerHeight,
        width: '100%',
        paddingLeft: mainPaddingLength,
        // backgroundColor: 'powderblue',
        borderRadius: 5,
    },
    teamSlot: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        height: '50%',
    },
    teamLogo: {
        height: '60%',
        aspectRatio: 1,
    },
    teamName: {
        marginLeft: '2%',
        color: 'black',
        // fontFamily: 'SFPro Display',
        fontWeight: '600',
        fontStyle: 'normal',
        fontSize: 20,
        lineHeight: 20,
    },
    teamRecord: {
        marginLeft: '2%',
        color: 'dimgray',
        // fontFamily: 'SFPro Display',
        fontWeight: '600',
        fontStyle: 'normal',
        lineHeight: 12,
        fontSize: 12,
    },
    teamScore: {
        position: 'absolute',
        right: '8%',
        color: 'black',
        // fontFamily: 'SFPro Display',
        fontWeight: '600',
        fontStyle: 'normal',
        fontSize: 18,
        lineHeight: 20,
    },
    bottomContainer: {
        height: bottomContainerHeight,
        width: '100%',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        paddingLeft: mainPaddingLength,
        borderTopWidth: 0.5,
        borderColor: 'lightgray',
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: 'skyblue',
    },
    gameTimeText: {
        color: 'black',
        // fontFamily: 'SFPro Display',
        fontWeight: '700',
        fontStyle: 'normal',
        lineHeight: 20,
        fontSize: 12,
    },
    participants: {
        position: 'absolute',
        right: '8%',
        color: 'black',
        // fontFamily: 'SFPro Display',
        fontWeight: '700',
        fontStyle: 'normal',
        lineHeight: 20,
        fontSize: 12,
    },
})
