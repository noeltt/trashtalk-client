import React from 'react'
import { View, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { messageCardStyle } from './style'

const profileIcon = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/user-3-line.svg'

export default MessageCard = ({ messageData }) => {
    let { timeText, message, username } = messageData

    return (
        <View style={messageCardStyle.container}>
            <View style={messageCardStyle.profileImageContainer}>
                <SvgUri style={messageCardStyle.profileImage} uri={profileIcon} />
            </View>
            <View style={messageCardStyle.messageContainer}>
                <View style={messageCardStyle.messageTopContainer}>
                    <View style={messageCardStyle.messageNameContainer}>
                        <Text style={messageCardStyle.messageNameText}>
                            {username || 'Username'}
                        </Text>
                    </View>
                    <View style={messageCardStyle.messageTimeContainer}>
                        <Text style={messageCardStyle.messageTimeText}>{timeText}</Text>
                    </View>
                </View>
                <View style={messageCardStyle.messageBottomContainer}>
                    <View style={messageCardStyle.messageTextContainer}>
                        <Text style={messageCardStyle.messageText}>{message}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}
