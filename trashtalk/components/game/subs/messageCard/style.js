import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    mainPaddingLength,
    navBarHeight,
} from '../../../../utils/constants'

const cardWidth = windowWidth - mainPaddingLength * 2,
    profileWidth = 30

export const messageCardStyle = StyleSheet.create({
    container: {
        minHeight: 40,
        width: cardWidth,
        marginBottom: 5,
        flexDirection: 'row',
        //transform: [{ scaleY: -1 }],

        // justifyContent: 'center',
        // alignItems: 'center',
        // borderWidth: 1,
        // borderColor: 'black',
        //backgroundColor: 'powderblue'
    },
    profileImageContainer: {
        width: profileWidth,
        maxWidth: profileWidth,
        paddingTop: 2,

        //backgroundColor: 'yellow'
    },
    profileImage: {
        minHeight: profileWidth,
        //maxHeight: profileWidth,
        minWidth: profileWidth,
        //aspectRatio: 1,
        //backgroundColor: 'white'
    },
    messageContainer: {
        width: cardWidth - profileWidth,

        // borderWidth: 2,
        // borderColor: 'black',
    },
    messageTopContainer: {
        minHeight: 25,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',

        //backgroundColor: 'powderblue'
    },
    messageNameContainer: {
        height: '100%',
        //justifyContent: 'flex-end'
    },
    messageNameText: {
        color: 'black',
        ////fontFamily: 'SFPro Display',
        fontSize: 16,
        fontWeight: '700',
    },
    messageTimeContainer: {
        height: '100%',
        marginLeft: 2,
    },
    messageTimeText: {
        color: 'gray',
        ////fontFamily: 'SFPro Display',
        fontSize: 12,
        lineHeight: 25,
        fontWeight: '700',
    },
    messageBottomContainer: {
        //backgroundColor: 'yellow'
    },
    messageTextContainer: {
        marginTop: -5,
        maxWidth: '100%',
    },
    messageText: {
        color: 'black',
        ////fontFamily: 'SFPro Display',
        fontSize: 14,
        fontWeight: '400',
    },
})
