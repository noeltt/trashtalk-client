import { StyleSheet } from 'react-native'
import { mainPaddingLength, windowWidth } from '../../../utils/constants'

export const commentCardStyle = StyleSheet.create({
    container: {
        // height: 100,
        width: windowWidth - mainPaddingLength * 2,
        marginBottom: 10,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',

        // borderWidth: 1,
        // borderColor: 'black',
        backgroundColor: '#F2F2F2',
    },
    commentCardLeft: {
        height: '100%',
        width: '13%',
        borderRadius: 5,
        alignItems: 'center',

        // backgroundColor: 'powderblue'
    },
    userImage: {
        width: '60%',
        marginTop: 5,
        aspectRatio: 1,
    },
    commentCardRight: {
        height: '100%',
        width: '87%',
        borderRadius: 5,
    },
    commentCardTop: {
        marginTop: 5,
        height: 20,
        width: '100%',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',

        // backgroundColor: 'pink'
    },
    usernameText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '600',
        fontSize: 12,
    },
    timeText: {
        // fontFamily: "SF Pro Display",
        marginRight: 5,
        position: 'absolute',
        right: 10,
        fontWeight: '600',
        fontSize: 10,
        color: 'gray',
    },
    commentCardMiddle: {
        // height: 40,
        width: '100%',
    },
    commentText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '400',
        fontSize: 10,
    },
    commentCardBottom: {
        marginTop: 5,
        height: 30,
        width: '100%',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
    },
    commentCardBottomButton: {
        height: '80%',
        width: 25,
        marginRight: 7,
        // backgroundColor: '#D7D7D7',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    buttonSvg: {
        minHeight: '40%',
        aspectRatio: 1,
    },
    replyStringText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '400',
        fontSize: 10,
        color: '#2D9CDB',
    },
    commentCardBottomRight: {
        height: '100%',
        position: 'absolute',
        right: 15,
        flexDirection: 'row',
        alignItems: 'center',

        // backgroundColor: 'powderblue'
    },
    bottomRightText: {
        marginRight: 5,
        // fontFamily: "SF Pro Display",
        fontWeight: '600',
        fontSize: 10,
    },
})
