import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import FastImage from 'react-native-fast-image'

import { blackLike, blackTrash, whiteLike, whiteTrash, blackComment } from '../../../utils/svgs'
import { getCountText, zeroIfLess } from '../../../utils/utils'
import {
    _unlikePostComment,
    _likePostComment,
    _untrashPostComment,
    _trashPostComment,
} from '../../../utils/api/postAPI'

import { commentCardStyle } from './style'

export default CommentCard = ({ commentData }) => {
    let {
        id: commentId,
        authorImageUrl,
        authorUsername,
        message,
        likeCount,
        trashCount,
        timeText,
        userLike,
        userTrash,
    } = commentData

    const [userLikeComment, setUserLikeComment] = useState(userLike)
    const [userTrashComment, setUserTrashComment] = useState(userTrash)
    const [commentLikeCount, setCommentLikeCount] = useState(likeCount)
    const [commentTrashCount, setCommentTrashCount] = useState(trashCount)
    // const [commentReactCount, setCommentReactCount] = useState(reactCount)

    let likeColors = userLikeComment
        ? {
              backgroundColor: '#2D9CDB',
              svg: whiteLike,
          }
        : {
              backgroundColor: '#D7D7D7',
              svg: blackLike,
          }

    let trashColors = userTrashComment
        ? {
              backgroundColor: '#DF2521',
              svg: whiteTrash,
          }
        : {
              backgroundColor: '#D7D7D7',
              svg: blackTrash,
          }

    const likeCommentChange = () => {
        if (userLikeComment) {
            setCommentLikeCount(zeroIfLess(commentLikeCount - 1))
            _unlikePostComment(commentId)
        } else {
            if (userTrashComment) {
                setCommentTrashCount(commentTrashCount - 1)
                setUserTrashComment(false)
            }

            setCommentLikeCount(commentLikeCount + 1)
            _likePostComment(commentId)
        }

        setUserLikeComment(!userLikeComment)
    }

    const trashCommentChange = () => {
        if (userTrashComment) {
            setCommentTrashCount(zeroIfLess(commentTrashCount - 1))
            _untrashPostComment(commentId)
        } else {
            if (userLikeComment) {
                setCommentLikeCount(commentLikeCount - 1)
                setUserLikeComment(false)
            }

            setCommentTrashCount(commentTrashCount + 1)
            _trashPostComment(commentId)
        }

        setUserTrashComment(!userTrashComment)
    }

    return (
        <View style={commentCardStyle.container}>
            <View style={commentCardStyle.commentCardLeft}>
                <FastImage
                    style={commentCardStyle.userImage}
                    source={{
                        uri: authorImageUrl,
                        priority: FastImage.priority.normal,
                    }}
                />
            </View>
            <View style={commentCardStyle.commentCardRight}>
                <View style={commentCardStyle.commentCardTop}>
                    <Text style={commentCardStyle.usernameText}>{authorUsername}</Text>
                    <Text style={commentCardStyle.timeText}>{timeText}</Text>
                </View>
                <View style={commentCardStyle.commentCardMiddle}>
                    <Text style={commentCardStyle.commentText}>{message}</Text>
                </View>
                <View style={commentCardStyle.commentCardBottom}>
                    <TouchableOpacity
                        style={[
                            commentCardStyle.commentCardBottomButton,
                            { backgroundColor: likeColors.backgroundColor },
                        ]}
                        onPress={likeCommentChange}
                    >
                        <SvgUri style={commentCardStyle.buttonSvg} uri={likeColors.svg} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            commentCardStyle.commentCardBottomButton,
                            { backgroundColor: trashColors.backgroundColor },
                        ]}
                        onPress={trashCommentChange}
                    >
                        <SvgUri style={commentCardStyle.buttonSvg} uri={trashColors.svg} />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            commentCardStyle.commentCardBottomButton,
                            { backgroundColor: '#D7D7D7' },
                        ]}
                    >
                        <SvgUri style={[commentCardStyle.buttonSvg]} uri={blackComment} />
                    </TouchableOpacity>
                    <Text style={commentCardStyle.replyStringText}>Show 3 replies</Text>
                    <View style={commentCardStyle.commentCardBottomRight}>
                        <Text style={commentCardStyle.bottomRightText}>
                            {getCountText(commentLikeCount)}
                        </Text>
                        <SvgUri
                            style={[commentCardStyle.buttonSvg, { marginRight: 3 }]}
                            uri={blackLike}
                        />
                        <Text style={commentCardStyle.bottomRightText}>
                            {getCountText(commentTrashCount)}
                        </Text>
                        <SvgUri style={commentCardStyle.buttonSvg} uri={blackTrash} />
                    </View>
                </View>
            </View>
        </View>
    )
}
