/* eslint-disable */
import { StyleSheet } from 'react-native'
import { mainPaddingLength, insetTop, windowWidth, windowHeight } from '../../../utils/constants'

export const postModalStyle = StyleSheet.create({
    container: {
        height: windowHeight - 200,
        width: windowWidth,
        // alignItems: 'center',
        //justifyContent: 'center',

        // marginTop: insetTop,

        backgroundColor: 'white',
    },
    settingsButton: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: mainPaddingLength,
        width: 80,
        height: 25,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#F2F2F2',
    },
    settingsText: {
        color: 'black',
        // fontFamily: 'LEMON MILK Pro FTR',
        fontWeight: '400',
        fontSize: 12,
    },
})
