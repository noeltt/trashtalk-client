/* eslint-disable */
import React, { useState, useEffect } from 'react'
import {
    Alert,
    Modal,
    StyleSheet,
    KeyboardAvoidingView,
    Keyboard,
    Text,
    View,
    TouchableOpacity,
} from 'react-native'

import { windowHeight } from '../../../utils/constants'

import { _createNewPost } from '../../../utils/api/postAPI'

import CreatePostHeader from '../subs/createPostHeader'
import CreatePostProfile from '../subs/createPostProfile'
import CreatePostBody from '../subs/createPostBody'
import CreatePollPostBody from '../subs/createPollPostBody'
import PostSettingsModal from '../subs/postSettingsModal'

import { postModalStyle } from './style'

export default createPostModal = ({ relevantIds, modalVisible, hideModal }) => {
    //const [readyToPost, setReadyToPost] = useState(false)
    const [postTitle, setPostTitle] = useState('')
    const [postType, setPostType] = useState(0)
    const [pollOptions, setPollOptions] = useState(['', ''])
    const [charLimit, setCharLimit] = useState(100)
    const [settingsModalVisible, setSettingsModalVisible] = useState(false)

    const [keyboardShowing, setKeyboardShowing] = useState(false)
    const [keyboardHeight, setKeyboardHeight] = useState(0)

    const readyToPost = () => {
        let isReady = postTitle.length > 0 && charLimit >= 0
        if (postType === 1) {
            console.log({ postType, pollOptions })
            let realOptionCount = 0

            pollOptions.forEach((option, i) => {
                if (option.length > 0) realOptionCount++
            })
            isReady = isReady && realOptionCount > 1
        }

        return isReady
    }

    const updatePostTitle = (text) => {
        setCharLimit(100 - text.length)
        setPostTitle(text)
    }

    const hideModalFunc = () => {
        setCharLimit(100)
        setPostTitle('')
        setPollOptions(['', ''])
        hideModal()
    }

    const showSettingsModal = () => setSettingsModalVisible(true)
    const hideSettingsModal = () => setSettingsModalVisible(false)
    const setSettingsModal = () => setSettingsModalVisible(!settingsModalVisible)

    //post type also determines post body for when poll is ready

    const createPost = async () => {
        if (readyToPost()) {
            const { gameId, playerId, teamId } = relevantIds
            const data = { postType, postTitle, pollOptions, gameId, playerId, teamId }
            // can do some load spinning here
            let { success } = await _createNewPost(data)

            if (success) {
                hideModal()
            } else {
                Alert.alert('Invalid Post', 'Something went wrong', [
                    { text: 'OK', onPress: () => {} },
                ])
            }
        } else {
            Alert.alert('Invalid Post', 'Cannot submit a blank post', [
                { text: 'OK', onPress: () => {} },
            ])
        }
    }

    useEffect(() => {
        const keyboardShowingListener = Keyboard.addListener('keyboardDidShow', (e) => {
            const tempKeyboardHeight = e.endCoordinates.height
            setKeyboardHeight(tempKeyboardHeight)
            setKeyboardShowing(true)
        })
        const keyboardHidingListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardHeight(0)
            setKeyboardShowing(false)
        })
        return () => {
            keyboardShowingListener.remove()
            keyboardHidingListener.remove()
        }
    }, [])

    const PostBody = postType === 0 ? CreatePostBody : CreatePollPostBody

    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => {}}
        >
            <PostSettingsModal
                setPostType={setPostType}
                postType={postType}
                settingsModalVisible={settingsModalVisible}
                hideSettingsModal={hideSettingsModal}
            />

            <CreatePostHeader
                readytoPost={readyToPost}
                hideModal={hideModalFunc}
                readyToPost={readyToPost}
                createPost={createPost}
            />
            <View
                onTouchStart={() => Keyboard.dismiss()}
                style={[
                    postModalStyle.container,
                    { marginTop: keyboardShowing && postType !== 0 ? -55 : 0 },
                ]}
            >
                <CreatePostProfile postType={postType} />
                <PostBody
                    charLimit={charLimit}
                    updatePostTitle={updatePostTitle}
                    pollOptions={pollOptions}
                    setPollOptions={setPollOptions}
                />
                <TouchableOpacity style={postModalStyle.settingsButton} onPress={showSettingsModal}>
                    <Text style={postModalStyle.settingsText}>Settings</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}
