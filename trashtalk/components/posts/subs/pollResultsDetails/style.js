/* eslint-disable */
import { StyleSheet } from 'react-native'
import { mainPaddingLength, windowWidth } from '../../../../utils/constants'

export const pollResultsDetailsStyle = StyleSheet.create({
    container: {
        marginTop: 5,
        // height: 100,
        width: '100%',
        paddingLeft: mainPaddingLength,
        // backgroundColor: 'powderblue'
    },
    resultsOptionContainer: {
        justifyContent: 'center',
        marginBottom: 10,
        height: 25,
        width: '100%',
    },
    resultsBarOuter: {
        height: '100%',
        alignItems: 'center',
        flexDirection: 'row',
        width: '95%',
        borderWidth: 0.5,
        borderColor: 'silver',
        borderRadius: 10,
    },
    resultsBarInner: {
        position: 'absolute',
        left: 0,
        alignItems: 'center',
        flexDirection: 'row',
        height: '100%',
        backgroundColor: '#FFB65066',
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
    },
    optionText: {
        left: 20,
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '500',
        fontSize: 12,
    },
    percentText: {
        position: 'absolute',
        right: 10,
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '500',
        fontSize: 12,
    },
    optionsBar: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '95%',
        borderWidth: 0.5,
        borderColor: 'black',
        borderRadius: 10,
    },
    icon: {
        position: 'absolute',
        right: 50,
        height: 5,
        width: 5,
        backgroundColor: 'green',
    },
})
