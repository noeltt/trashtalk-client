/* eslint-disable */
import React, { useState } from 'react'
import { View, Text, TextInput, FlatList, TouchableOpacity } from 'react-native'

import { pollResultsDetailsStyle } from './style'

export default postPollResults = ({
    postPollOptions,
    userVoted,
    updateUserVote,
    userVote,
    votesCount,
}) => {
    const renderResultItem = ({ item }) => {
        const percent = Math.floor(((item.voteCount || 0) / votesCount) * 100)
        const endBorderRadius = percent > 99 ? 10 : 0
        const percentString = `${percent}%`

        return (
            <View style={pollResultsDetailsStyle.resultsOptionContainer}>
                <View style={pollResultsDetailsStyle.resultsBarOuter}>
                    <View
                        style={[
                            pollResultsDetailsStyle.resultsBarInner,
                            {
                                width: `${percent}%`,
                                borderTopRightRadius: endBorderRadius,
                                borderBottomRightRadius: endBorderRadius,
                            },
                        ]}
                    ></View>
                    <Text style={pollResultsDetailsStyle.optionText}>{item.name}</Text>
                    {userVote === item.id && <View style={pollResultsDetailsStyle.icon} />}
                    <Text style={pollResultsDetailsStyle.percentText}>{percentString}</Text>
                </View>
            </View>
        )
    }

    const renderOptionItem = ({ item }) => {
        const theUserVoted = () => updateUserVote(item.id)

        return (
            <View style={pollResultsDetailsStyle.resultsOptionContainer}>
                <TouchableOpacity style={pollResultsDetailsStyle.optionsBar} onPress={theUserVoted}>
                    <Text>{item.name}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <FlatList
            data={postPollOptions}
            renderItem={userVoted ? renderResultItem : renderOptionItem}
            keyExtractor={(item, index) => index}
            style={pollResultsDetailsStyle.container}
        />
    )
}
