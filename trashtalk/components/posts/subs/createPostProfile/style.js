/* eslint-disable */
import { StyleSheet } from 'react-native'
import { headerHeight, mainPaddingLength, windowWidth } from '../../../../utils/constants'

export const createPostBodyStyle = StyleSheet.create({
    container: {
        //height: 300,
        width: windowWidth,
        marginTop: 20,

        //backgroundColor: 'powderblue'
    },
    textInput: {
        paddingLeft: mainPaddingLength,
        maxWidth: windowWidth,
    },
    charLimitText: {
        marginTop: 10,
        paddingLeft: mainPaddingLength,
        fontSize: 12,
        color: 'silver',
    },
})
