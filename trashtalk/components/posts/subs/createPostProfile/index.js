/* eslint-disable */
import React, { useState } from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import FastImage from 'react-native-fast-image'
import { SvgUri } from 'react-native-svg'

import { createPostProfileStyle } from './style'
import { postHeaderStyle } from '../../postHeader/style'

import { adminLogo, postDiscussionLogo, postPollLogo } from '../../../../utils/svgs'

const defaultImageUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/defaultprofile.png'

export default createPostProfile = ({ postType }) => {
    let isAdmin = true
    let authorUsername = global.username

    let postLogo = postType === 0 ? postDiscussionLogo : postPollLogo
    let postBackgroundColor = postType === 0 ? 'lightblue' : '#6FCF97'
    let postFontcolor = postType === 0 ? 'cornflowerblue' : '#219653'
    let postTypeText = postType === 0 ? 'Discussion' : 'Poll'

    return (
        <View style={[postHeaderStyle.container]}>
            <View style={postHeaderStyle.userImageContainer}>
                <FastImage
                    style={postHeaderStyle.userImage}
                    source={{
                        uri: defaultImageUrl,
                        priority: FastImage.priority.normal,
                    }}
                />
            </View>
            <View style={postHeaderStyle.postHeaderCenter}>
                <View style={postHeaderStyle.postHeaderTop}>
                    {isAdmin && <SvgUri style={postHeaderStyle.profileLogo} uri={adminLogo} />}
                    <Text style={postHeaderStyle.profileUsernameText}>{authorUsername}</Text>
                </View>
                <View style={postHeaderStyle.postHeaderBottom}>
                    <Text style={postHeaderStyle.profileTimeText}>Author</Text>
                </View>
            </View>
            <View style={[postHeaderStyle.postLabel, { backgroundColor: postBackgroundColor }]}>
                <SvgUri style={postHeaderStyle.postLabelLogo} uri={postLogo} />
                <Text style={[postHeaderStyle.postLabelText, { color: postFontcolor }]}>
                    {postTypeText}
                </Text>
            </View>
        </View>
    )
}
