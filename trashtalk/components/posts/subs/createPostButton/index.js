/* eslint-disable */
import React, { useState } from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import CreatePostModal from '../../createPostModal'

import { whitePlus } from '../../../../utils/svgs'

import { createPostButtonStyle } from './style'

export default createPostButton = ({ gameId, teamId, playerId }) => {
    const [modalVisible, setModalVisible] = useState(false)
    const relevantIds = {
        gameId: gameId || 0,
        teamId: teamId || 0,
        playerId: playerId || 0,
    }

    const hideModal = () => {
        setModalVisible(false)
    }

    const showModal = () => {
        setModalVisible(true)
    }

    return (
        <>
            <CreatePostModal
                relevantIds={relevantIds}
                modalVisible={modalVisible}
                hideModal={hideModal}
            />
            <TouchableOpacity style={createPostButtonStyle.container} onPress={showModal}>
                <SvgUri style={createPostButtonStyle.createPostButtonSvg} uri={whitePlus} />
                <Text style={createPostButtonStyle.createPostButtonText}>Create Post</Text>
            </TouchableOpacity>
        </>
    )
}
