/* eslint-disable */
import { StyleSheet } from 'react-native'
import { headerHeight, mainPaddingLength, windowWidth } from '../../../../utils/constants'

export const createPostButtonStyle = StyleSheet.create({
    container: {
        height: 40,
        width: windowWidth - 2 * mainPaddingLength,
        marginTop: 10,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5,

        backgroundColor: 'black',
    },
    createPostButtonSvg: {},
    createPostButtonText: {
        //fontFamily: 'LEMON MILK Pro FTR',
        marginLeft: 10,
        fontSize: 16,
        fontWeight: 'bold',
        fontStyle: 'normal',
        color: 'white',
    },
})
