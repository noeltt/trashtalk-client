/* eslint-disable */
import React, { useState } from 'react'
import { View, Text, TextInput, FlatList, TouchableOpacity } from 'react-native'

import { createPollPostStyle } from './style'

const questionPlaceholder = 'Add question'
const optionPlaceholder = 'Add option'
const optionCharLimit = 30

export default createPollPostBody = ({
    charLimit,
    updatePostTitle,
    pollOptions,
    setPollOptions,
}) => {
    const charLimitBackgroundColor = charLimit >= 0 ? 'white' : 'red'

    const updateOption = (text, index) => {
        // console.log({text, index})
        let tempOptions = [...pollOptions]
        tempOptions[index] = text
        setPollOptions([...tempOptions])
    }

    const renderItem = ({ item, index }) => (
        <>
            <Text style={createPollPostStyle.subHeader}>Option</Text>
            <TextInput
                placeholder={optionPlaceholder}
                style={createPollPostStyle.textInput}
                maxLength={25}
                onChangeText={(text) => updateOption(text, index)}
            />
        </>
    )

    const addOption = () => {
        if (pollOptions.length < 4) {
            let tempOptions = pollOptions
            tempOptions = [...tempOptions, '']
            setPollOptions([...tempOptions])
        }
    }

    return (
        <View style={createPollPostStyle.container}>
            <View style={createPollPostStyle.subContainer}>
                <Text style={createPollPostStyle.subHeader}>Question</Text>
                <TextInput
                    placeholder={questionPlaceholder}
                    style={createPollPostStyle.textInput}
                    onChangeText={updatePostTitle}
                    multiline={true}
                />
            </View>
            <Text
                style={[
                    createPollPostStyle.charLimitText,
                    { backgroundColor: charLimitBackgroundColor },
                ]}
            >
                Character Limit: {charLimit}
            </Text>
            <FlatList
                data={pollOptions}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
                style={createPollPostStyle.optionContainer}
            />
            <TouchableOpacity style={createPollPostStyle.addOptionContainer} onPress={addOption}>
                <Text style={createPollPostStyle.addOptionText}>Add Option</Text>
            </TouchableOpacity>
        </View>
    )
}
