/* eslint-disable */
import { StyleSheet } from 'react-native'
import { headerHeight, windowWidth, mainPaddingLength } from '../../../../utils/constants'

export const createPollPostStyle = StyleSheet.create({
    container: {
        //height: 300,
        width: windowWidth,
        //borderWidth: 1,
        borderColor: 'black',
        // border: 'powderblue'
    },
    subContainer: {
        paddingLeft: mainPaddingLength,
        paddingRight: mainPaddingLength,
    },
    optionContainer: {
        paddingLeft: mainPaddingLength,
        paddingRight: mainPaddingLength,
        marginTop: 30,
    },
    subHeader: {
        color: 'black',
        // fontFamily: 'LEMON MILK Pro FTR',
        fontWeight: '700',
        fontSize: 16,
        marginTop: 20,
    },
    textInput: {
        // fontFamily: 'LEMON MILK Pro FTR',
        alignItems: 'center',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 19,
        paddingBottom: 5,
        borderBottomWidth: 0.5,
        borderColor: 'silver',
    },
    charLimitText: {
        marginTop: 3,
        paddingLeft: mainPaddingLength,
        fontSize: 12,
        color: 'silver',
    },
    addOptionContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: mainPaddingLength,
        width: 80,
        height: 25,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: '#F2F2F2',
    },
    addOptionText: {
        color: 'black',
        // fontFamily: 'LEMON MILK Pro FTR',
        fontWeight: '400',
        fontSize: 12,
    },
})
