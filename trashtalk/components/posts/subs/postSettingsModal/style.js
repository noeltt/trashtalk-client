/* eslint-disable */
import { StyleSheet } from 'react-native'
import { windowWidth, windowHeight, insetTop, mainPaddingLength } from '../../../../utils/constants'

export const postSettingsModalStyle = StyleSheet.create({
    backgroundContainer: {
        height: windowHeight,
        width: windowWidth,
        // alignItems: 'center',
        //justifyContent: 'center',

        marginTop: insetTop,
    },
    container: {
        height: 100,
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderColor: 'black',
        borderRadius: 5,
        width: windowWidth,
        paddingLeft: mainPaddingLength,
        position: 'absolute',
        bottom: 0,
    },
    text: {
        marginTop: 5,
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontWeight: '500',
        fontSize: 15,
    },
})
