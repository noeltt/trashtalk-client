/* eslint-disable */
import React, { useState } from 'react'
import {
    Alert,
    Modal,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
} from 'react-native'
import { postSettingsModalStyle } from './style'

export default postSettingsModal = ({
    postType,
    setPostType,
    settingsModalVisible,
    hideSettingsModal,
}) => {
    const changePostText = postType === 0 ? 'Create A Poll' : 'Create A Discussion'

    const changePostType = () => {
        const newPostType = postType === 0 ? 1 : 0
        setPostType(newPostType)
    }

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={settingsModalVisible}
            onRequestClose={() => {}}
        >
            <TouchableOpacity
                style={postSettingsModalStyle.backgroundContainer}
                onPress={hideSettingsModal}
            >
                <View style={postSettingsModalStyle.container}>
                    <TouchableOpacity style={postSettingsModalStyle.text} onPress={changePostType}>
                        <Text style={postSettingsModalStyle.text}>{changePostText}</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        </Modal>
    )
}
