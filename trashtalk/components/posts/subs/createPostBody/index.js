/* eslint-disable */
import React, { useState } from 'react'
import { View, Text, TextInput } from 'react-native'

import { textInputPlaceholder } from '../../../../utils/constants'

import { createPostBodyStyle } from '../createPostProfile/style'

export default createPostBody = ({ charLimit, updatePostTitle }) => {
    const charLimitBackgroundColor = charLimit >= 0 ? 'white' : 'red'

    return (
        <View style={createPostBodyStyle.container}>
            <TextInput
                style={createPostBodyStyle.textInput}
                onChangeText={updatePostTitle}
                placeholder={textInputPlaceholder}
                multiline={true}
            />
            <Text
                style={[
                    createPostBodyStyle.charLimitText,
                    { backgroundColor: charLimitBackgroundColor },
                ]}
            >
                Character Limit: {charLimit}
            </Text>
        </View>
    )
}
