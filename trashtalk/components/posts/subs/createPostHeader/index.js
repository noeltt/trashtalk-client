/* eslint-disable */
import React, { useState } from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { whiteEx } from '../../../../utils/svgs'
import { createPostHeaderStyle } from './style'

export default createPostHeader = ({ createPost, readyToPost, hideModal }) => {
    let postTextColor = readyToPost ? '#FFB650' : 'gray'
    let activeOpacity = readyToPost ? 0.2 : 1

    const submitPost = () => {
        if (readyToPost) {
            createPost()
        }
    }

    return (
        <View style={createPostHeaderStyle.container}>
            <TouchableOpacity style={createPostHeaderStyle.closeContainer} onPress={hideModal}>
                <SvgUri style={createPostHeaderStyle.closeSvg} uri={whiteEx} />
            </TouchableOpacity>
            <Text style={createPostHeaderStyle.text}>Create Post</Text>
            <TouchableOpacity
                activeOpacity={activeOpacity}
                style={createPostHeaderStyle.createPostButtonContainer}
                onPress={submitPost}
            >
                <Text
                    style={[createPostHeaderStyle.createPostButtonText, { color: postTextColor }]}
                >
                    Post
                </Text>
            </TouchableOpacity>
        </View>
    )
}
