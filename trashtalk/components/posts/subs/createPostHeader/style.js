/* eslint-disable */
import { StyleSheet } from 'react-native'
import { headerHeight, windowWidth, insetTop } from '../../../../utils/constants'

export const createPostHeaderStyle = StyleSheet.create({
    container: {
        height: headerHeight,
        width: windowWidth,
        //flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: insetTop,
        zIndex: 10,

        backgroundColor: 'black',
    },
    closeContainer: {
        position: 'absolute',
        left: '4%',
        justifyContent: 'center',
        alignItems: 'center',
        height: headerHeight,
    },
    closeSvg: {
        minHeight: '30%',
        aspectRatio: 1,
    },
    text: {
        // fontFamily: 'LEMON MILK Pro FTR',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        color: 'white',
    },
    createPostButtonContainer: {
        position: 'absolute',
        right: '4%',
        justifyContent: 'center',
        alignItems: 'center',
        height: headerHeight,
    },
    createPostButtonText: {
        // fontFamily: 'SF Pro Display',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 16,
    },
})

//text colors
