/* eslint-disable */
import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import {
    _likePost,
    _trashPost,
    _unlikePost,
    _untrashPost,
    _votePostPoll,
} from '../../../utils/api/postAPI'

import { getCountText, zeroIfLess } from '../../../utils/utils'
import {
    whiteComment,
    whiteLike,
    clearLike,
    clearTrash,
    blackLike,
    blackTrash,
    whiteTrash,
} from '../../../utils/svgs'

import PollResultsDetails from '../subs/pollResultsDetails'

import { postDetailStyle } from './style'

const onColors = {
    backgroundColor: 'gray',
    textColor: 'white',
    logo: whiteLike,
}

const offColors = {
    backgroundColor: '#F2F2F2',
    textColor: 'black',
    logo: blackLike,
}

const onColorsTrash = { ...onColors, logo: whiteTrash }
const offColorsTrash = { ...offColors, logo: blackTrash }

export default PostDetail = ({ postBodyData }) => {
    let {
        postId = 0,
        postTitle = '',
        trashCount = 0,
        likeCount = 0,
        commentCount = 0,
        postType = 0,
        userLike = false,
        userTrash = false,
        pollOptions,
        didUserVote,
        userVoteOption,
        totalVoteCount,
    } = postBodyData

    const isPoll = postType === 1

    const [userLikePost, setUserLikePost] = useState(userLike)
    const [userTrashPost, setUserTrashPost] = useState(userTrash)
    const [postLikeCount, setPostLikeCount] = useState(likeCount)
    const [postTrashCount, setPostTrashCount] = useState(trashCount)
    const [userVoted, setUserVoted] = useState(didUserVote)
    const [userVote, setUserVote] = useState(userVoteOption)
    const [votesCount, setVotesCount] = useState(totalVoteCount || 0)
    const [postPollOptions, setPostPollOptions] = useState([...pollOptions])

    let userLikeColors = userLikePost ? onColors : offColors
    let userTrashColors = userTrashPost ? onColorsTrash : offColorsTrash

    // would love api stuff to happen on unmount instead of actual click
    const changeLike = () => {
        if (userLikePost) {
            setPostLikeCount(zeroIfLess(postLikeCount - 1))
            _unlikePost(postId)
        } else {
            setPostLikeCount(postLikeCount + 1)
            if (userTrashPost) {
                setPostTrashCount(zeroIfLess(postTrashCount - 1))
                setUserTrashPost(false)
            }

            _likePost(postId)
        }

        setUserLikePost(!userLikePost)
    }

    const changeTrash = () => {
        if (userTrashPost) {
            setPostTrashCount(postTrashCount - 1)
            _untrashPost(postId)
        } else {
            setPostTrashCount(postTrashCount + 1)
            if (userLikePost) {
                setPostLikeCount(zeroIfLess(postLikeCount - 1))
                setUserLikePost(false)
            }
            _trashPost(postId)
        }

        setUserTrashPost(!userTrashPost)
    }

    const updateUserVote = (optionId) => {
        _votePostPoll(postId, optionId)

        setUserVoted(true)
        setUserVote(optionId)
        setVotesCount(votesCount + 1)

        let tempPollOptions = [...postPollOptions]
        if (tempPollOptions[optionId - 1].voteCount === undefined) {
            tempPollOptions[optionId - 1].voteCount = 0
        }
        tempPollOptions[optionId - 1].voteCount++

        setPostPollOptions([...tempPollOptions])
    }

    return (
        <View style={postDetailStyle.container}>
            <View style={postDetailStyle.postTextContainer}>
                <Text style={postDetailStyle.postText}>{postTitle}</Text>
            </View>
            {isPoll && (
                <PollResultsDetails
                    style={postDetailStyle.pollResultsContainer}
                    postPollOptions={postPollOptions}
                    votesCount={votesCount}
                    updateUserVote={updateUserVote}
                    userVoted={userVoted}
                    userVote={userVote}
                />
            )}
            <View style={postDetailStyle.bottomRow}>
                <View style={postDetailStyle.bottomDetailsContainer}>
                    <SvgUri style={postDetailStyle.bottomDetailsLabelLogo} uri={clearLike} />
                    <Text style={postDetailStyle.bottomDetailsText}>
                        {getCountText(postLikeCount)}
                    </Text>
                    <SvgUri style={postDetailStyle.bottomDetailsLabelLogo} uri={clearTrash} />
                    <Text style={postDetailStyle.bottomDetailsText}>
                        {getCountText(postTrashCount)}
                    </Text>
                    <SvgUri style={postDetailStyle.bottomDetailsLabelLogo} uri={whiteComment} />
                    <Text style={postDetailStyle.bottomDetailsText}>
                        {getCountText(commentCount)}
                    </Text>
                </View>
                <View style={postDetailStyle.bottomInteractionContainer}>
                    <TouchableOpacity
                        style={[
                            postDetailStyle.bottomInteractionTile,
                            { backgroundColor: userLikeColors.backgroundColor },
                        ]}
                        onPress={changeLike}
                    >
                        <SvgUri
                            style={postDetailStyle.bottomDetailsLabelLogo}
                            uri={userLikeColors.logo}
                        />
                        <Text
                            style={[
                                postDetailStyle.bottomInteractionTileText,
                                { color: userLikeColors.textColor },
                            ]}
                        >
                            Like
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[
                            postDetailStyle.bottomInteractionTile,
                            { backgroundColor: userTrashColors.backgroundColor },
                        ]}
                        onPress={changeTrash}
                    >
                        <SvgUri
                            style={postDetailStyle.bottomDetailsLabelLogo}
                            uri={userTrashColors.logo}
                        />
                        <Text
                            style={[
                                postDetailStyle.bottomInteractionTileText,
                                { color: userTrashColors.textColor },
                            ]}
                        >
                            Trash
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
