import { StyleSheet } from 'react-native'
import { mainPaddingLength } from '../../../utils/constants'

export const postDetailStyle = StyleSheet.create({
    container: {
        marginTop: 20,
        width: '100%',
        marginBottom: 10,

        // borderWidth: 2,
        // borderColor: 'blue'
    },

    postTextContainer: {
        paddingLeft: mainPaddingLength,
        paddingRight: mainPaddingLength,
        width: '100%',
        // backgroundColor: 'powderblue'
    },
    postText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '600',
        fontSize: 16,
    },
    bottomRow: {
        marginTop: 15,
        height: 20,
        width: '100%',
        flexDirection: 'row',
    },
    bottomDetailsContainer: {
        paddingLeft: mainPaddingLength,
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',

        // backgroundColor: 'powderblue'
    },
    bottomDetailsLabelLogo: {
        marginRight: 5,
        minHeight: '50%',
        // maxHeight: '50%',
        aspectRatio: 1,
    },
    bottomDetailsText: {
        marginRight: 15,
        // fontFamily: "SF Pro Display",
        fontWeight: '400',
        fontSize: 10,
    },
    bottomInteractionContainer: {
        height: '100%',
        width: 100,
        position: 'absolute',
        right: 0,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    bottomInteractionTile: {
        paddingLeft: 5,
        height: '100%',
        // width: 28,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 3,
        marginRight: 10,
    },
    bottomInteractionTileText: {
        marginRight: 5,
        // fontFamily: "LEMON Milk Pro FTR",
        fontWeight: 'bold',
        fontSize: 8,
    },
})
