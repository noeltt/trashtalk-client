import { StyleSheet } from 'react-native'
import { mainPaddingLength, windowWidth } from '../../../utils/constants'

const postHeaderHeight = 60

export const postHeaderStyle = StyleSheet.create({
    container: {
        height: postHeaderHeight,
        width: windowWidth,
        paddingLeft: mainPaddingLength,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        // borderWidth: 2,
        // borderColor: 'gray'
        // backgroundColor: 'pink'
    },
    userImageContainer: {
        height: postHeaderHeight,
        aspectRatio: 1,
    },
    userImage: {
        height: '100%',
        width: '100%',
    },
    postHeaderCenter: {
        height: '80%',
        width: '60%',
        marginLeft: '2%',
    },
    postHeaderTop: {
        height: '50%',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor: 'powderblue'
    },
    profileLogo: {
        maxHeight: '75%',
        marginRight: '1%',
        aspectRatio: 1,
    },
    profileUsernameText: {
        // fontFamily: "Lemon milk pro ftr",
        fontWeight: '600',
        fontSize: 16,
        color: 'black',
    },

    postHeaderBottom: {
        height: '50%',
        width: '100%',
        justifyContent: 'center',
        // backgroundColor: 'yellow'
    },
    profileTimeText: {
        marginLeft: '1.5%',
        // fontFamily: "SF Pro Display",
        fontWeight: '400',
        fontSize: 10,
        color: 'gray',
    },
    postLabel: {
        height: 26,
        // width: 100,
        position: 'absolute',
        right: 20,
        bottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 10,
    },
    postLabelLogo: {
        marginLeft: 10,
        marginRight: 5,
        minHeight: '50%',
        aspectRatio: 1,
    },
    postLabelText: {
        marginRight: 10,
        // fontFamily: "SF Pro Display",
        fontWeight: '500',
        fontSize: 12,
    },
})
