/* eslint-disable */
import React from 'react'
import { View, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import FastImage from 'react-native-fast-image'

import { postHeaderStyle } from './style'
import { adminLogo, postDiscussionLogo, postPollLogo } from '../../../utils/svgs'

export default PostHeader = ({ postHeaderData }) => {
    let {
        authorImageUrl = '',
        authorUsername = '',
        isAdmin = false,
        postType = 0,
        timeText = '',
    } = postHeaderData

    let postLogo = postType === 0 ? postDiscussionLogo : postPollLogo
    let postBackgroundColor = postType === 0 ? 'lightblue' : '#6FCF97'
    let postFontcolor = postType === 0 ? 'cornflowerblue' : '#219653'
    let postTypeText = postType === 0 ? 'Discussion' : 'Poll'

    return (
        <View style={postHeaderStyle.container}>
            <View style={postHeaderStyle.userImageContainer}>
                <FastImage
                    style={postHeaderStyle.userImage}
                    source={{
                        uri: authorImageUrl,
                        priority: FastImage.priority.normal,
                    }}
                />
            </View>
            <View style={postHeaderStyle.postHeaderCenter}>
                <View style={postHeaderStyle.postHeaderTop}>
                    {isAdmin && <SvgUri style={postHeaderStyle.profileLogo} uri={adminLogo} />}
                    <Text style={postHeaderStyle.profileUsernameText}>{authorUsername}</Text>
                </View>
                <View style={postHeaderStyle.postHeaderBottom}>
                    <Text style={postHeaderStyle.profileTimeText}>{timeText}</Text>
                </View>
            </View>
            <View style={[postHeaderStyle.postLabel, { backgroundColor: postBackgroundColor }]}>
                <SvgUri style={postHeaderStyle.postLabelLogo} uri={postLogo} />
                <Text style={[postHeaderStyle.postLabelText, { color: postFontcolor }]}>
                    {postTypeText}
                </Text>
            </View>
        </View>
    )
}
