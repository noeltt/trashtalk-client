import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useNavigation } from '@react-navigation/native'

import { iaHeaderStyle } from './style'

import { logoWhite, logoTextWhite, whiteBackArrow } from '../../../utils/svgs'

export default Header = () => {
    const navigation = useNavigation()

    const goBack = () => {
        navigation.goBack()
    }

    return (
        <View style={iaHeaderStyle.container}>
            <TouchableOpacity style={iaHeaderStyle.backArrowContainer} onPress={goBack}>
                <SvgUri style={iaHeaderStyle.backArrowSvg} uri={whiteBackArrow} />
            </TouchableOpacity>
            <View style={iaHeaderStyle.logoContainer}>
                <View style={iaHeaderStyle.logoImageContainer}>
                    <SvgUri style={iaHeaderStyle.logoImage} uri={logoWhite} />
                </View>
                <View style={iaHeaderStyle.logoTextContainer}>
                    <SvgUri style={iaHeaderStyle.logoTextImage} uri={logoTextWhite} />
                </View>
            </View>
        </View>
    )
}
