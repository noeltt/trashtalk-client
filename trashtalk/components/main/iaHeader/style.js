import { StyleSheet } from 'react-native'
import { windowWidth, headerHeight } from '../../../utils/constants'

const logoHeight = 35
const logoWidth = 180

export const iaHeaderStyle = StyleSheet.create({
    container: {
        height: headerHeight,
        width: windowWidth,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        // borderWidth: 2,
        // borderColor: 'blue',
    },
    backArrowContainer: {
        position: 'absolute',
        left: '3%',
        justifyContent: 'center',
        alignItems: 'center',
        height: headerHeight,
        width: 30,
        // backgroundColor: 'blue'
    },
    backArrowSvg: {
        minHeight: '30%',
        aspectRatio: 1,
    },
    logoContainer: {
        height: logoHeight,
        width: logoWidth,
        // borderWidth: 2,
        // borderColor: 'blue',
        flexDirection: 'row',
    },
    logoImageContainer: {
        height: '100%',
        width: logoHeight,
        // backgroundColor: 'powderblue'
    },
    logoImage: {
        maxHeight: '100%',
        maxWidth: '100%',
    },
    logoTextContainer: {
        height: '100%',
        width: logoWidth - logoHeight,
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'blue'
    },
    logoTextImage: {
        maxHeight: '100%',
        maxWidth: '100%',
    },
})
