/* eslint-disable */
import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import FastImage from 'react-native-fast-image'
import { useNavigation } from '@react-navigation/native'

import { statsTableRowStyle } from './style'

export default statsTableRow = ({ children, colorIndex }) => {
    const bgColors = ['white', '#F4F4F4']

    return (
        <View style={[statsTableRowStyle.container, { backgroundColor: bgColors[colorIndex] }]}>
            {children}
        </View>
    )
}
