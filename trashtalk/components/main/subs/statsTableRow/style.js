/* eslint-disable */
import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength } from '../../../../utils/constants'

const rowHeight = 35

export const statsTableRowStyle = StyleSheet.create({
    container: {
        height: rowHeight,
        width: '100%',
        //paddingLeft: mainPaddingLength,

        flexDirection: 'row',
        alignItems: 'center',

        //borderWidth: .5
    },
})
