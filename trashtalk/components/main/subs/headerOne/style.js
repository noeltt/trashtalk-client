import { StyleSheet } from 'react-native'

export const headerOneStyle = StyleSheet.create({
    gridHeader: {
        flexDirection: 'row',
        height: 60,
        width: '100%',
        // marginTop: 5,
        backgroundColor: 'white',
    },
    gridBannerTextContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%',
        width: '50%',
        marginRight: 0,
        marginLeft: 0,
    },
})
