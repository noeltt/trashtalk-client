// chat subview

import React from 'react'
import { View, Text } from 'react-native'

import { mainStyles } from '../../../../styles/main'
import { headerOneStyle } from './style'

export default class SubHeaderOne extends React.Component {
    render() {
        let title = this.props.title || 'Filler'

        return (
            <View style={[headerOneStyle.gridHeader, this.props.style]}>
                <View style={headerOneStyle.gridBannerTextContainer}>
                    <Text style={mainStyles.textSizeOne}>{title}</Text>
                </View>
                {this.props.children}
            </View>
        )
    }
}
