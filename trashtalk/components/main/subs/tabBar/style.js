import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    gameRoomHeaderHeight,
    headerHeight,
} from '../../../../utils/constants'

export const tabBarStyle = StyleSheet.create({
    container: {
        height: footerHeight,
        borderTopWidth: 1,
        borderColor: 'lightgray',
        //backgroundColor: 'powderblue'
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    iconContainer: {
        height: footerHeight,
        width: footerHeight,
        justifyContent: 'center',
        // borderWidth: 2,
        // borderColor: 'black',
        marginHorizontal: 13,
    },
    iconImageContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '50%',
        width: '100%',
        // borderWidth: 2,
        // borderColor: 'black',
        // backgroundColor: 'powderblue'
    },
    iconImage: {
        // borderWidth: 1,
        // borderColor: 'black',
        resizeMode: 'contain',
        height: '100%',
        transform: [{ scale: 1.1 }],
    },
    iconTextContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: '30%',
        width: '100%',
        // borderWidth: 2,
        // borderColor: 'black'
    },
    iconText: {
        ////fontFamily: 'SFPro Display',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 14,
        color: 'black',
    },
})
