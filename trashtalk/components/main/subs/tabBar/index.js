import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { tabBarStyle } from './style'

import {
    homeWhite,
    chatWhite,
    searchWhite,
    profileWhite,
    homeBlack,
    chatBlack,
    searchBlack,
    profileBlack,
} from '../../../../utils/svgs'

export default TabBar = ({ state, descriptors, navigation }) => {
    const footerTiles = () => {
        return state.routes.map((route, index) => {
            const { options } = descriptors[route.key]
            const label =
                options.tabBarLabel !== undefined
                    ? options.tabBarLabel
                    : options.title !== undefined
                    ? options.title
                    : route.name

            const isFocused = state.index === index

            const onPress = () => {
                const event = navigation.emit({
                    type: 'tabPress',
                    target: route.key,
                })

                if (label === 'LiveChat') {
                    // console.log('navigate to LiveChat home')
                    navigation.navigate('Home', {
                        screen: 'LiveChat',
                        params: {
                            view: 'default',
                        },
                    })
                } else if (!isFocused && !event.defaultPrevented) {
                    navigation.navigate(
                        { name: route.name, merge: true },
                        {
                            view: 'default',
                        }
                    )
                }
            }

            const onLongPress = () => {
                navigation.emit({
                    type: 'tabLongPress',
                    target: route.key,
                })
            }

            let navLabel = 'Home'

            if (label === 'HomeScreen') {
                navIconUrl = isFocused ? homeBlack : homeWhite
            } else if (label === 'LiveChat') {
                navLabel = 'Live Chat'
                navIconUrl = isFocused ? chatBlack : chatWhite
            } else if (label === 'Search') {
                navLabel = label
                navIconUrl = isFocused ? searchBlack : searchWhite
            } else if (label === 'Profile') {
                navLabel = label
                navIconUrl = isFocused ? profileBlack : profileWhite
            } else {
                return
            }

            return (
                <TouchableOpacity
                    key={route.key}
                    style={tabBarStyle.iconContainer}
                    onPress={onPress}
                    onLongPress={onLongPress}
                >
                    <View style={tabBarStyle.iconImageContainer}>
                        <SvgUri tabBarStyle={tabBarStyle.iconImage} uri={navIconUrl} />
                    </View>
                    <View style={tabBarStyle.iconTextContainer}>
                        <Text style={tabBarStyle.iconText}>{navLabel}</Text>
                    </View>
                </TouchableOpacity>
            )
        })
    }

    return <View style={tabBarStyle.container}>{footerTiles()}</View>
}

// function MyTabBar({ state, descriptors, navigation }) {
//     return (
//       <View style={{ flexDirection: 'row',backgroundColor:'#F4AF5F',height:50,borderRadius:50,justifyContent:'center',alignItems:'center' }}>
//         {state.routes.map((route, index) => {
//           const { options } = descriptors[route.key];
//           const label =
//             options.tabBarLabel !== undefined
//               ? options.tabBarLabel
//               : options.title !== undefined
//               ? options.title
//               : route.name;

//           const isFocused = state.index === index;

//           const onPress = () => {
//             const event = navigation.emit({
//               type: 'tabPress',
//               target: route.key,
//             });

//             if (!isFocused && !event.defaultPrevented) {
//               navigation.navigate(route.name);
//             }
//           };

//           const onLongPress = () => {
//             navigation.emit({
//               type: 'tabLongPress',
//               target: route.key,
//             });
//           };

//           return (
//             <TouchableOpacity
//               accessibilityRole='button'
//               accessibilityStates={isFocused ? ['selected'] : []}
//               accessibilityLabel={options.tabBarAccessibilityLabel}
//               testID={options.tabBarTestID}
//               onPress={onPress}
//               onLongPress={onLongPress}
//               style={{ flex: 1, alignItems:'center' }}
//             >
//               <Text style={{ color: isFocused ? '#673ab7' : '#222' }}>
//                 {label}
//               </Text>
//             </TouchableOpacity>
//           );
//         })}
//       </View>
//     );
//   }
