import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength, textInputHeight } from '../../../../utils/constants'

export const textInputStyle = StyleSheet.create({
    container: {
        width: windowWidth,
        height: textInputHeight,
        paddingLeft: mainPaddingLength,
        borderTopWidth: 0.5,
        borderBottomWidth: 0.5,
        borderColor: 'lightgray',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'white',
    },
    messageInput: {
        height: '100%',
        width: windowWidth - (mainPaddingLength + textInputHeight),
        // //fontFamily: 'SFPro Display',
        fontWeight: '500',
        fontSize: 16,
        lineHeight: 19,

        // borderWidth: 2,
        // borderColor: 'black'
    },
    messageInputButtonContainer: {
        height: '100%',
        maxHeight: '100%',
        aspectRatio: 1,
        alignItems: 'center',
        justifyContent: 'center',

        // backgroundColor: 'gray'
    },
    messageInputButton: {
        maxHeight: '45%',
        aspectRatio: 1,
    },
})
