import React, { useState } from 'react'
import { View, TextInput, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { _createPostComment } from '../../../../utils/api/postAPI'
import { inputText } from '../../../../utils/svgs'

import { textInputStyle } from './style'

const messagePlaceholder = 'Talk your sh*t'

export default IATextInput = ({ postId, refreshComments }) => {
    const [commentMessage, setCommentMessage] = useState('')

    const submitComment = async () => {
        // console.log({ commentMessage })
        // console.log({ postId })

        await _createPostComment(postId, commentMessage)

        setCommentMessage('')
        refreshComments(postId)
    }

    return (
        <View style={textInputStyle.container}>
            <TextInput
                placeholder={messagePlaceholder}
                onChangeText={(message) => setCommentMessage(message)}
                onSubmitEditing={submitComment}
                value={commentMessage}
                style={textInputStyle.messageInput}
            />

            <TouchableOpacity
                style={textInputStyle.messageInputButtonContainer}
                onPress={submitComment}
            >
                <SvgUri style={textInputStyle.messageInputButton} uri={inputText} />
            </TouchableOpacity>
        </View>
    )
}
