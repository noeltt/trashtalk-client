//chat subview

import React, { useEffect, useState } from 'react'
import { View, ScrollView, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { mainStyles } from '../../../../styles/main'
import { headerOneButtonStyle } from './style'

const headerOneButtonUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/headerlisticon.svg'

export default class SubHeaderOneButton extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <TouchableOpacity style={headerOneButtonStyle.gridBannerDDButton}>
                <SvgUri style={headerOneButtonStyle.gridBannerSvg} uri={headerOneButtonUrl} />
            </TouchableOpacity>
        )
    }
}
