import React from 'react'
import { StyleSheet } from 'react-native'

export const headerOneButtonStyle = StyleSheet.create({
    gridBannerDDButton: {
        height: '100%',
        width: 60,
        position: 'absolute',
        right: 20,
        marginRight: 0,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'skyblue'
    },
    gridBannerSvg: {
        minHeight: '30%',
        minWidth: '30%',
    },
})
