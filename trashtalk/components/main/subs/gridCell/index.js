import '../../../../utils/globalManager'

import React from 'react'
import { TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import { gridCellStyle } from './style'

export default GridCell = ({ postData }) => {
    const navigation = useNavigation()

    const goToPost = () => {
        let { postId } = postData
        navigation.push('Home', { screen: 'Posts', params: { postId } })
    }

    return (
        <TouchableOpacity style={gridCellStyle.gridCell} onPress={goToPost}>
            {/* <ImageBackground style={gridCellStyle.gridImage} source={{uri: postData.image_url}} >
                <View style={gridCellStyle.gridCellTextContainer}>
                    <Text style={gridCellStyle.gridCellText}>{title}</Text>
                </View>
            </ImageBackground>     */}
        </TouchableOpacity>
    )
}
