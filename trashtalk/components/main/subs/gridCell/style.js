import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
} from '../../../../utils/constants'

const rowHeight = 150

export const gridCellStyle = StyleSheet.create({
    gridCell: {
        height: rowHeight,
        width: rowHeight,
        marginRight: 20,
        backgroundColor: 'gray',

        // borderWidth: 2,
        // borderColor: 'red',
    },
    gridImage: {
        height: '100%',
        width: '100%',
        resizeMode: 'cover',
    },
    gridCellTextContainer: {
        marginTop: '70%',
        height: '20%',
        width: '85%',
        marginLeft: '8%',
        //backgroundColor: 'black',
        justifyContent: 'center',
    },
    gridCellText: {
        //fontFamily: 'LEMON MILK Pro FTR',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
    },
})
