/* eslint-disable */
import React from 'react'
import { View, ScrollView, Text, ImageBackground } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

const colorList = ['transparent', 'black']

export default BlackGradientLayer = ({ children }) => {
    return (
        <LinearGradient
            style={{
                height: '100%',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
            }}
            colors={['transparent', 'black']}
            start={{ x: 0.5, y: 0 }}
        >
            {children}
        </LinearGradient>
    )
}
