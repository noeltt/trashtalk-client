import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength } from '../../../../utils/constants'

const postCardHeight = 240
const postCardWidth = windowWidth - mainPaddingLength * 2
const topHeight = 180
const bottomHeight = postCardHeight - topHeight
const borderRadius = 10
const iconWidth = 30

export const postCardStyle = StyleSheet.create({
    container: {
        height: postCardHeight,
        width: postCardWidth,
        borderRadius: borderRadius,
        marginTop: 5,
        marginBottom: 10,
        // backgroundColor: 'powderblue'
    },
    topContainer: {
        height: topHeight,
        width: postCardWidth,
        borderTopRightRadius: borderRadius,
        borderTopLeftRadius: borderRadius,

        backgroundColor: 'gray',
    },
    postImage: {
        height: '100%',
        width: '100%',
        borderTopRightRadius: borderRadius,
        borderTopLeftRadius: borderRadius,
        resizeMode: 'cover',
    },
    postTitleContainer: {
        // marginLeft: 5
    },
    postTitleText: {
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 16,
    },
    postDateText: {
        color: 'gray',
        // fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 12,

        position: 'absolute',
        right: 15,
    },
    bottomContainer: {
        height: bottomHeight,
        width: postCardWidth,
        borderBottomRightRadius: borderRadius,
        borderBottomLeftRadius: borderRadius,

        borderWidth: 0.5,
        borderColor: 'lightgray',
    },
    bottomTopContainer: {
        height: '50%',
        width: postCardWidth,
        flexDirection: 'row',
        alignItems: 'center',
    },
    bottomBottomContainer: {
        height: '50%',
        width: postCardWidth,
        borderBottomRightRadius: borderRadius,
        borderBottomLeftRadius: borderRadius,
        flexDirection: 'row',
        alignItems: 'center',

        // borderWidth: .5,
        // borderColor: 'black'
    },
    iconContainerOne: {
        height: '100%',
        width: iconWidth,
        alignItems: 'center',
        justifyContent: 'center',

        // backgroundColor: 'powderblue'
    },
    logoOne: {
        minHeight: '50%',
        aspectRatio: 1,
    },
    logoTwo: {
        marginRight: 3,
        minHeight: '50%',
        aspectRatio: 1,
    },
    iconContainerTwo: {
        height: '100%',
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',

        backgroundColor: 'gray',
    },
    bottomText: {
        marginRight: 10,
        color: 'black',
        // fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 12,
    },
})
