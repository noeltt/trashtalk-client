import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import FastImage from 'react-native-fast-image'
import { useNavigation } from '@react-navigation/native'

import { postCardStyle } from './style'

import {
    whiteComment,
    viewsLogo,
    fireLogo,
    postDiscussionLogo,
    postPollLogo,
} from '../../../../utils/svgs'
import { getTimeText } from '../../../../utils/utils'

export default PostCard = (props) => {
    const navigation = useNavigation()

    let { postData } = props
    let {
        postType,
        imageUrl: postImageUrl,
        title,
        minDiff,
        commentCount,
        viewCount,
        points,
        postId,
    } = postData // { likeCount }
    let isHot = points > 50
    let postLogoUrl = postType === 0 ? postDiscussionLogo : postPollLogo

    title = title.substr(0, 17) + '...'

    const goToPost = () => {
        navigation.push('Home', { screen: 'Posts', params: { postId } })
    }

    return (
        <TouchableOpacity style={postCardStyle.container} onPress={goToPost}>
            <View style={postCardStyle.topContainer}>
                <FastImage
                    style={postCardStyle.postImage}
                    source={{
                        uri: postImageUrl,
                        priority: FastImage.priority.high,
                    }}
                />
            </View>
            <View style={postCardStyle.bottomContainer}>
                <View style={postCardStyle.bottomTopContainer}>
                    <View style={postCardStyle.iconContainerOne}>
                        <SvgUri style={postCardStyle.logoOne} uri={postLogoUrl} />
                    </View>
                    <Text style={postCardStyle.postTitleText}>{title}</Text>
                    <Text style={postCardStyle.postDateText}>{getTimeText(minDiff)}</Text>
                </View>
                <View style={postCardStyle.bottomBottomContainer}>
                    <View style={postCardStyle.iconContainerOne}>
                        {isHot && <SvgUri style={postCardStyle.logoOne} uri={fireLogo} />}
                    </View>

                    <SvgUri style={postCardStyle.logoTwo} uri={whiteComment} />

                    <Text style={postCardStyle.bottomText}>{commentCount}</Text>

                    <SvgUri style={postCardStyle.logoTwo} uri={viewsLogo} />

                    <Text style={postCardStyle.bottomText}>{viewCount}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}
