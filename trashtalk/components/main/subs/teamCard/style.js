/* eslint-disable */
import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength } from '../../../../utils/constants'

const postCardHeight = 240
const postCardWidth = windowWidth - mainPaddingLength * 2
const topHeight = 180
const bottomHeight = postCardHeight - topHeight
const borderRadius = 10
const iconWidth = 30
const innerMargin = 10

export const teamCardStyle = StyleSheet.create({
    container: {
        height: postCardHeight,
        width: postCardWidth,
        borderRadius: borderRadius,
        marginTop: 5,
        marginBottom: 10,

        //backgroundColor: 'powderblue'
    },
    topContainer: {
        height: topHeight,
        width: postCardWidth,
        borderTopRightRadius: borderRadius,
        borderTopLeftRadius: borderRadius,

        backgroundColor: 'gray',
    },
    teamImage: {
        height: '100%',
        width: '100%',
        borderTopRightRadius: borderRadius,
        borderTopLeftRadius: borderRadius,
        resizeMode: 'cover',
    },
    teamTitleText: {
        color: 'black',
        //fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 15,
    },
    teamFollowButton: {
        position: 'absolute',
        right: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: '100%',

        //backgroundColor: 'powderblue'
    },
    teamFollowText: {
        marginLeft: 3,
        //color: '#0B8A00',
        // fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 14,
    },
    bottomContainer: {
        height: bottomHeight,
        width: postCardWidth,
        paddingLeft: innerMargin,
        borderBottomRightRadius: borderRadius,
        borderBottomLeftRadius: borderRadius,

        borderWidth: 0.5,
        borderColor: 'lightgray',
    },
    bottomTopContainer: {
        height: '60%',
        width: postCardWidth,
        flexDirection: 'row',
        alignItems: 'center',
    },
    bottomBottomContainer: {
        height: '40%',
        width: postCardWidth,
        borderBottomRightRadius: borderRadius,
        borderBottomLeftRadius: borderRadius,
        flexDirection: 'row',
        // alignItems: 'center',

        // borderWidth: .5,
        // borderColor: 'black'
    },
    followersSvg: {
        maxHeight: '80%',
        aspectRatio: 1,
    },
    commentCountContainer: {
        marginRight: 10,
    },
    bottomText: {
        marginLeft: 5,
        color: 'black',
        //fontFamily: "//SF Pro Display",
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 12,
    },
})
