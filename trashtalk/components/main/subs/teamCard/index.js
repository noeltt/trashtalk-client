/* eslint-disable */
import React, { useState } from 'react'
import FastImage from 'react-native-fast-image'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useNavigation } from '@react-navigation/native'

import { _followNBATeam, _unfollowNBATeam } from '../../../../utils/api/statsAPI'

import { getCountText } from '../../../../utils/utils'

import { followersBlack, redMinus, greenPlus } from '../../../../utils/svgs'

import { teamCardStyle } from './style'

let time = '2d ago'

const FollowTouchable = ({ teamFollow }) => (
    <TouchableOpacity style={teamCardStyle.teamFollowButton} onPress={teamFollow}>
        <SvgUri style={teamCardStyle.followersSvg} uri={greenPlus} />
        <Text style={[teamCardStyle.teamFollowText, { color: '#0B8A00' }]}>Follow </Text>
    </TouchableOpacity>
)

const UnfollowTouchable = ({ teamFollow }) => (
    <TouchableOpacity style={teamCardStyle.teamFollowButton} onPress={teamFollow}>
        <SvgUri style={teamCardStyle.followersSvg} uri={redMinus} />
        <Text style={[teamCardStyle.teamFollowText, { color: '#E32521' }]}>Unfollow </Text>
    </TouchableOpacity>
)

export const TeamCard = ({ teamData }) => {
    const [following, setFollowing] = useState(teamData.following)
    const [followCount, setFollowCount] = useState(teamData.followCount)
    const navigation = useNavigation()

    const goToTeamProfile = () => {
        navigation.push('Home', { screen: 'TeamProfile', params: { teamId: teamData.teamId } })
    }

    const teamFollow = async () => {
        if (following) {
            await _unfollowNBATeam(teamData.teamId)
            setFollowCount(followCount - 1)
        } else {
            await _followNBATeam(teamData.teamId)
            setFollowCount(followCount + 1)
        }

        setFollowing(!following)
    }

    let FollowTouch = following ? UnfollowTouchable : FollowTouchable

    return (
        <View style={teamCardStyle.container}>
            <TouchableOpacity style={teamCardStyle.topContainer} onPress={goToTeamProfile}>
                <FastImage
                    style={teamCardStyle.teamImage}
                    source={{
                        uri: teamData.imageUrl,
                        priority: FastImage.priority.high,
                    }}
                />
            </TouchableOpacity>
            <View style={teamCardStyle.bottomContainer}>
                <View style={teamCardStyle.bottomTopContainer}>
                    <Text style={teamCardStyle.teamTitleText}>{teamData.fullName}</Text>
                    <FollowTouch teamFollow={teamFollow} />
                </View>
                <View style={teamCardStyle.bottomBottomContainer}>
                    <SvgUri style={teamCardStyle.followersSvg} uri={followersBlack} />
                    <Text style={teamCardStyle.bottomText}>
                        {getCountText(followCount)} Followers
                    </Text>
                </View>
            </View>
        </View>
    )
}
