import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
} from '../../../utils/constants'

const boxHeight = 45,
    filterHeight = 50,
    containerHeight = boxHeight + filterHeight,
    containerMargin = 15,
    containerWidth = windowWidth - containerMargin * 2

export const searchHeaderStyle = StyleSheet.create({
    container: {
        height: containerHeight,
        width: containerWidth,
        marginTop: containerMargin,
        // borderWidth: 2,
        // borderColor: 'black',
        //backgroundColor: 'black'
    },
    searchContainer: {
        height: boxHeight,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: 'gray',
        borderRadius: 10,
    },
    searchBoxContainer: {
        height: '80%',
        width: '100%',
        flexDirection: 'row',
        //backgroundColor: 'gray'
    },
    searchButtonContainer: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'yellow'
    },
    searchButtonImg: {
        maxHeight: '55%',
        minHeight: '55%',
        aspectRatio: 1,
    },
    searchInput: {
        padding: 0,
        height: '100%',
        width: containerWidth,
        ////fontFamily: 'SFPro Display',
        fontWeight: '400',
        fontSize: 17,
        lineHeight: 19,

        // borderWidth: 2,
        // borderColor: 'black'
    },
    searchFilterContainer: {
        height: filterHeight,
        width: '100%',
        //backgroundColor: 'gray',
        flexDirection: 'row',
        alignItems: 'center',
    },
    filterButton: {
        height: 30,
        width: '30%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginRight: containerMargin,
        borderRadius: 5,
        backgroundColor: 'lightgray',
    },
    filterButtonText: {
        //fontFamily: 'LEMON MILK Pro FTR',
        color: 'black',
        fontWeight: 'bold',
        fontStyle: 'normal',
    },
    filterButtonChosen: {
        height: 30,
        width: '30%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginRight: containerMargin,
        borderRadius: 5,
        backgroundColor: 'black',
    },
    filterTextChosen: {
        //fontFamily: 'LEMON MILK Pro FTR',
        color: 'white',
        fontWeight: 'bold',
        fontStyle: 'normal',
    },
})
