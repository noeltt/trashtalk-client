import React, { useEffect, useState, useCallback } from 'react'
import { View, ScrollView, Text, TextInput, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { debounce } from 'lodash'

import { mainStyles } from '../../../styles/main'
import { searchHeaderStyle } from './style'

const searchIcon = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/search-line.svg',
    searchPlaceholder = 'Search'

export default SearchHeader = ({ searchType, setSearchType, setSearchText }) => {
    const delayedOnChange = debounce((inputText) => {
        setSearchText(inputText)
    }, 500)

    const updateSearchType = (searchTypeOption) => {
        setSearchType(searchTypeOption)
    }

    return (
        <View style={searchHeaderStyle.container}>
            <View style={searchHeaderStyle.searchContainer}>
                <View style={searchHeaderStyle.searchBoxContainer}>
                    <TouchableOpacity style={searchHeaderStyle.searchButtonContainer}>
                        <SvgUri style={searchHeaderStyle.searchButtonImg} uri={searchIcon} />
                    </TouchableOpacity>
                    <TextInput
                        onChangeText={delayedOnChange}
                        placeholder={searchPlaceholder}
                        style={searchHeaderStyle.searchInput}
                    />
                </View>
            </View>
            <View style={searchHeaderStyle.searchFilterContainer}>
                <TouchableOpacity
                    onPress={() => {
                        updateSearchType(1)
                    }}
                    style={
                        searchType === 1
                            ? searchHeaderStyle.filterButtonChosen
                            : searchHeaderStyle.filterButton
                    }
                >
                    <Text
                        style={
                            searchType === 1
                                ? searchHeaderStyle.filterTextChosen
                                : searchHeaderStyle.filterButtonText
                        }
                    >
                        Teams
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={searchHeaderStyle.filterButton}>
                    <Text style={searchHeaderStyle.filterButtonText}>Players</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        updateSearchType(3)
                    }}
                    style={
                        searchType === 3
                            ? searchHeaderStyle.filterButtonChosen
                            : searchHeaderStyle.filterButton
                    }
                >
                    <Text
                        style={
                            searchType === 3
                                ? searchHeaderStyle.filterTextChosen
                                : searchHeaderStyle.filterButtonText
                        }
                    >
                        Users
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
