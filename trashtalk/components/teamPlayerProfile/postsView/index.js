/* eslint-disable */
import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, FlatList } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'

import { _getPosts } from '../../../utils/api/postAPI'

import PostCard from '../../main/subs/postCard'
import CreatePostButton from '../../posts/subs/createPostButton'

import { teamPlayerPostsViewStyle } from './style'

export default teamPlayerPostsView = ({ teamId, playerId }) => {
    const [posts, setPosts] = useState([])

    const renderItem = ({ item }) => <PostCard postData={item} />

    useEffect(() => {
        const getPosts = async () => {
            const urlQuery = `playerId=${playerId}`
            const ret = await _getPosts(urlQuery)
            if (ret) {
                const { posts } = ret
                setPosts(posts)
            } else {
                console.log('client error getting recent posts')
            }
        }

        getPosts()
    }, [])

    return (
        <View style={teamPlayerPostsViewStyle.container}>
            <FlatList
                data={posts}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
                style={teamPlayerPostsViewStyle.postsContainer}
            />
            <CreatePostButton teamId={teamId} playerId={playerId} />
        </View>
    )
}
