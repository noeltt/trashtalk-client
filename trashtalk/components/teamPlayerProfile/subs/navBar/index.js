/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { gameNavStyle } from '../../../game/subs/gameRoomNav/style'

export default teamPlayerProfileNavBar = ({ navigateView }) => {
    const changeView = (viewType) => navigateView(viewType)

    return (
        <View style={gameNavStyle.container}>
            <TouchableOpacity
                style={gameNavStyle.navButtonContainerHalf}
                onPress={() => {
                    changeView(1)
                }}
            >
                <Text style={gameNavStyle.navText}>Posts</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={gameNavStyle.navButtonContainerHalf}
                onPress={() => {
                    changeView(2)
                }}
            >
                <Text style={gameNavStyle.navText}>Stats</Text>
            </TouchableOpacity>
        </View>
    )
}
