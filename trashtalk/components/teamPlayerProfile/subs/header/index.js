/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, ImageBackground } from 'react-native'
import FastImage from 'react-native-fast-image'

import BlackGradient from '../../../main/subs/blackGradient'

import { teamPlayerHeaderStyle } from './style'

export default teamPlayerProfileHeader = ({ backgroundUrl, headshotUrl, headerName }) => {
    return (
        <ImageBackground
            style={teamPlayerHeaderStyle.container}
            source={{ uri: backgroundUrl }}
            resizeMode="cover"
        >
            <BlackGradient>
                <FastImage
                    style={teamPlayerHeaderStyle.headshotImg}
                    source={{
                        uri: headshotUrl,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />
                <Text style={teamPlayerHeaderStyle.playerText}>{headerName}</Text>
            </BlackGradient>
        </ImageBackground>
    )
}
