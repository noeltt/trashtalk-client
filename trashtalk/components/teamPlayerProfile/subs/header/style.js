/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    profileHeaderHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

export const teamPlayerHeaderStyle = StyleSheet.create({
    container: {
        height: profileHeaderHeight,
        width: windowWidth,

        backgroundColor: 'black',
    },
    headshotImg: {
        height: '90%',
        aspectRatio: 3 / 2,
        position: 'absolute',
        bottom: 0,
        left: 0,
    },
    playerText: {
        color: 'white',
        // fontFamily: 'LEMON MILK Pro FTR',
        fontWeight: 'bold',
        fontSize: 16,

        position: 'absolute',
        bottom: mainPaddingLength,
        right: mainPaddingLength,
    },
})
