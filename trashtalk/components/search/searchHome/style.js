import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../utils/constants'

export const searchLobbyStyle = StyleSheet.create({
    container: {
        height: windowHeight - headerHeight,
        width: windowWidth,
        paddingLeft: mainPaddingLength,
        backgroundColor: 'white',

        // borderWidth: 1,
        // borderColor: 'blue'
    },

    // User List stuff

    userContainer: {
        height: 50,
        width: '97%',
        marginVertical: 5,

        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: 'lightgray',

        // borderWidth: 1,
        // borderColor: 'blue',
    },
    userProfileImage: {
        height: '70%',
        aspectRatio: 1,
        resizeMode: 'contain',
    },
    userText: {
        // fontFamily: "//SF Pro Display",
        marginLeft: 15,
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 15,
    },
    userFollowingButton: {
        height: 30,
        width: 30,

        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

        marginLeft: 'auto',
        marginRight: 10,

        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'blue',
    },
    userFollowingSvg: {
        minHeight: '45%',
        aspectRatio: 1,
    },
})
