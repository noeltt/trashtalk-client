/* eslint-disable */
import React, { useEffect, useState, useCallback } from 'react'
import { Alert, View, FlatList, Text, TouchableOpacity, ImageBackground } from 'react-native'
import { SvgUri } from 'react-native-svg'
import FastImage from 'react-native-fast-image'
import { useNavigation } from '@react-navigation/native'

import SearchHeader from '../../main/searchHeader'
import { TeamCard } from '../../main/subs/teamCard'
import SubHeaderOne from '../../main/subs/headerOne'
import SubHeaderOneButton from '../../main/subs/headerOneButton'

import { _getNBATeams } from '../../../utils/api/statsAPI'
import { _getUsersByUsername, _followUser, _unfollowUser } from '../../../utils/api/userAPI'

import { greenPlus, redMinus } from '../../../utils/svgs'
import { mainStyles } from '../../../styles/main'
import { searchLobbyStyle } from './style'
import { getProfileUrl } from '../../../utils/tempProfiles'

export const SearchLobby = (props) => {
    const [searchType, setSearchType] = useState(1)
    const [searchText, setSearchText] = useState('')

    const updateSearchType = (searchTypeNum) => {
        setSearchText('')
        setSearchType(searchTypeNum)
    }

    const SearchList = searchType === 1 ? TeamList : UserList

    return (
        <View style={searchLobbyStyle.container}>
            <SearchHeader
                searchType={searchType}
                setSearchType={updateSearchType}
                setSearchText={setSearchText}
            />
            <SearchList searchText={searchText} />
        </View>
    )
}

const TeamList = ({ searchText }) => {
    const [teams, setTeams] = useState([])
    const [fullTeams, setFullTeams] = useState([])

    useEffect(() => {
        const setTeamResults = async () => {
            // const { data } =
            let { teams } = await _getNBATeams()
            setTeams(teams)
            setFullTeams(teams)
        }

        setTeamResults()
    }, [])

    useEffect(() => {
        // console.log({ teams })
        const tempTeams = fullTeams.filter((x) =>
            String(x.fullName).toLowerCase().includes(searchText.toLowerCase())
        )

        setTeams([...tempTeams])
    }, [searchText])

    const renderItem = ({ item }) => <TeamCard key={item.key} teamData={item} />

    return (
        <FlatList
            //onScroll={scrollEvent}
            data={teams}
            maxToRenderPerBatch={10}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
        />
    )
}

const UserList = ({ searchText }) => {
    const [users, setUsers] = useState([])

    const renderItem = ({ item }) => <UserRow item={item} />

    useEffect(() => {
        const setUserResults = async () => {
            const { users } = await _getUsersByUsername(searchText)
            setUsers(users)
        }

        setUserResults()
    }, [searchText])

    return (
        <FlatList
            //onScroll={scrollEvent}
            data={users}
            maxToRenderPerBatch={10}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
        />
    )
}

const UserRow = ({ item }) => {
    const [following, setFollowing] = useState(item.following)
    const navigation = useNavigation()

    const gotoProfile = (uid) => {
        navigation.push('Home', { screen: 'Profile', params: { profileUid: uid } })
    }

    const FollowingButton = following ? NotFollowingTouchable : FollowingTouchable
    const profileImageUrl = getProfileUrl()

    const changeFollowing = async () => {
        const data = {
            followingUid: item.uid,
        }
        if (following) {
            Alert.alert('LMAO', `You're now unfollowing ${item.username}`, [
                { text: 'OK', onPress: () => {} },
            ])
            await _unfollowUser(data)
        } else {
            Alert.alert('Congrats', `You're now following ${item.username}`, [
                { text: 'OK', onPress: () => {} },
            ])
            await _followUser(data)
        }
        setFollowing(!following)
    }

    return (
        <TouchableOpacity
            onPress={() => {
                gotoProfile(item.uid)
            }}
            style={searchLobbyStyle.userContainer}
            key={item.key}
        >
            <FastImage
                style={searchLobbyStyle.userProfileImage}
                source={{
                    uri: item.avatar_url ?? profileImageUrl,
                    priority: FastImage.priority.high,
                }}
            />
            <Text style={searchLobbyStyle.userText}>{item.username}</Text>
            <FollowingButton changeFollowing={changeFollowing} />
        </TouchableOpacity>
    )
}

const FollowingTouchable = ({ changeFollowing }) => {
    return (
        <TouchableOpacity onPress={changeFollowing} style={[searchLobbyStyle.userFollowingButton]}>
            <SvgUri style={searchLobbyStyle.userFollowingSvg} uri={greenPlus} />
        </TouchableOpacity>
    )
}

const NotFollowingTouchable = ({ changeFollowing }) => {
    return (
        <TouchableOpacity onPress={changeFollowing} style={[searchLobbyStyle.userFollowingButton]}>
            <SvgUri style={searchLobbyStyle.userFollowingSvg} uri={redMinus} />
        </TouchableOpacity>
    )
}
