import { StyleSheet } from 'react-native'

export const registrationVerificationStyle = StyleSheet.create({
    svgContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 200,
        width: '100%',

        // borderWidth: 2,
        // borderColor: 'black',
    },
    svg: {
        minHeight: '50%',
        aspectRatio: 1,

        // borderWidth: 2,
        // borderColor: 'blue',
    },
})
