import React, { useState } from 'react'
import { TouchableOpacity, View, Text, TextInput, Alert } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { registrationUserDataStyle } from '../userData/style'
import { registrationVerificationStyle } from './style'
import { loginStyle } from '../../../screens/login/style'
import { regVerify } from '../../../utils/svgs'
import { _verifyRegistrationCode } from '../../../utils/api/userAPI'

export default RegistrationCodeVerification = ({ gotoStageThree }) => {
    const [registrationCode, setRegistrationCode] = useState('')

    const badRegistrationCode = (message) =>
        Alert.alert('Invalid Registration', message, [{ text: 'OK', onPress: () => {} }])

    const verifyRegistrationCode = async () => {
        // await check registration code
        //
        if (registrationCode.length === 0) {
            badRegistrationCode('Please submit a code')
            return
        }

        const registrationCodeSuccess = await _verifyRegistrationCode({ code: registrationCode })

        if (registrationCodeSuccess) {
            gotoStageThree()
        } else {
            badRegistrationCode('Please submit a code')
        }
    }

    return (
        <View style={registrationUserDataStyle.container}>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'black' }]}>
                Check your email.
            </Text>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'gray' }]}>
                Look for a verification code
            </Text>
            <View style={registrationVerificationStyle.svgContainer}>
                <SvgUri style={registrationVerificationStyle.svg} uri={regVerify} />
            </View>
            <View style={[loginStyle.inputWindow]}>
                <Text style={loginStyle.inputLabel}>Verification Code</Text>
                <TextInput
                    value={registrationCode}
                    onChangeText={(registrationCode) => setRegistrationCode(registrationCode)}
                    style={[loginStyle.inputText, { textAlign: 'center' }]}
                />
            </View>
            <TouchableOpacity style={loginStyle.loginButton} onPress={verifyRegistrationCode}>
                <Text style={loginStyle.loginButtonText}>Verify</Text>
            </TouchableOpacity>
        </View>
    )
}
