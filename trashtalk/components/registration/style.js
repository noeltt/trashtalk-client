import { StyleSheet } from "react-native"
import { windowHeight, 
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight } from "../../utils/constants"

export const registrationStyle = StyleSheet.create({
    container: {
        height: windowHeight - (headerHeight),
        width: windowWidth,
        paddingLeft: mainPaddingLength,
        backgroundColor: 'white',

        borderWidth: 1,
        borderColor: 'blue'
    },
})