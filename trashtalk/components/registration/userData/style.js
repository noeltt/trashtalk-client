import { StyleSheet } from 'react-native'
import { windowHeight, windowWidth, mainPaddingLength } from '../../../utils/constants'

export const registrationUserDataStyle = StyleSheet.create({
    container: {
        width: windowWidth,
        padding: mainPaddingLength,
        paddingTop: 40,

        flexDirection: 'column',
        alignItems: 'flex-start',

        // borderWidth: 1,
        // borderColor: 'blue',
    },
    textInputStyle: {
        width: windowWidth - 2 * mainPaddingLength,
        height: 20,
        fontSize: 16,
        lineHeight: 19,

        marginTop: 10,

        borderBottomWidth: 1,
        borderColor: 'lightgray',
    },
    textStyle: {
        // fontFamily: 'LEMON MILK Pro FTR',
        // textAlign: 'left',
        fontWeight: '700',
        fontSize: 19,
        marginBottom: 10,
    },
    // color: 'black',
    // // fontFamily: 'LEMON MILK Pro FTR',
    // fontWeight: '400',
    // fontSize: 17,
})
