import React, { useState } from 'react'
import { TouchableOpacity, View, Text, TextInput, Alert } from 'react-native'
import { registrationUserDataStyle } from './style'
import { loginStyle } from '../../../screens/login/style'
import { isEmailValid } from '../../../utils/utils'
import { _createUser } from '../../../utils/api/userAPI'

export default RegistrationUserData = ({ gotoStageTwo }) => {
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    const badRegistration = (message) =>
        Alert.alert('Invalid Registration', message, [{ text: 'OK', onPress: () => {} }])

    const createUser = async () => {
        // console.log({ email, username, password, confirmPassword })
        if (password !== confirmPassword) {
            badRegistration('Password do not match')
        } else if (email.length === 0 || username.length === 0 || password.length === 0) {
            badRegistration('Please fill in all fields')
        } else if (isEmailValid(email.toLowerCase())) {
            const data = {
                email: email.toLowerCase(),
                username,
                password,
            }

            const userCreateSuccess = await _createUser(data)
            // console.log({ userCreateSuccess })

            if (!userCreateSuccess) {
                badRegistration('The username or email are already taken, please try again')
            } else {
                gotoStageTwo()
                console.log('success')
            }
        } else {
            badRegistration('Please submit a valid email')
        }
        // gotoStageTwo()
    }

    return (
        <View style={registrationUserDataStyle.container}>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'black' }]}>
                Don't be soft!
            </Text>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'gray' }]}>
                Create your account here
            </Text>
            <View style={[loginStyle.inputWindow]}>
                <Text style={loginStyle.inputLabel}>Email Address</Text>
                <TextInput onChangeText={(text) => setEmail(text)} style={loginStyle.inputText} />
            </View>
            <View style={[loginStyle.inputWindow]}>
                <Text style={loginStyle.inputLabel}>Username</Text>
                <TextInput
                    onChangeText={(text) => setUsername(text)}
                    style={loginStyle.inputText}
                />
            </View>
            <View style={[loginStyle.inputWindow]}>
                <Text style={loginStyle.inputLabel}>Password</Text>
                <TextInput
                    onChangeText={(text) => setPassword(text)}
                    secureTextEntry={true}
                    style={loginStyle.inputText}
                />
            </View>
            <View style={[loginStyle.inputWindow]}>
                <Text style={loginStyle.inputLabel}>Confirm Password</Text>
                <TextInput
                    onChangeText={(text) => setConfirmPassword(text)}
                    secureTextEntry={true}
                    style={loginStyle.inputText}
                />
            </View>
            <TouchableOpacity style={loginStyle.loginButton} onPress={createUser}>
                <Text style={loginStyle.loginButtonText}>Submit</Text>
            </TouchableOpacity>
        </View>
    )
}
