import React, { useState } from 'react'
import { TouchableOpacity, View, Text, TextInput, Alert } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { registrationUserDataStyle } from '../userData/style'
import { loginStyle } from '../../../screens/login/style'
import { registrationVerificationStyle } from '../codeVerification/style'
import { regWelcome } from '../../../utils/svgs'

import { useNavigation } from '@react-navigation/native'

export default RegistrationWelcome = () => {
    const navigation = useNavigation()

    const goHome = () => {
        navigation.goBack()
    }

    return (
        <View style={registrationUserDataStyle.container}>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'black' }]}>Welcome.</Text>
            <Text style={[registrationUserDataStyle.textStyle, { color: 'gray' }]}>
                Guess you're not soft after all
            </Text>
            <View style={[registrationVerificationStyle.svgContainer, { marginTop: '30%' }]}>
                <SvgUri style={registrationVerificationStyle.svg} uri={regWelcome} />
            </View>
            <TouchableOpacity
                style={[loginStyle.loginButton, { marginTop: '40%' }]}
                onPress={goHome}
            >
                <Text style={loginStyle.loginButtonText}>Continue to Login</Text>
            </TouchableOpacity>
        </View>
    )
}
