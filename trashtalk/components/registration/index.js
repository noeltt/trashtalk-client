import React, { useState, useEffect } from 'react'
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native'
import Header from '../main/iaHeader'
import { registrationStyle } from './style'

import RegistrationUserData from './userData'
import RegistrationCode from './codeVerification'
import RegistrationWelcome from './welcome'

export default Registration = () => {
    const [registrationStage, setRegistrationStage] = useState(1)

    const [keyboardShowing, setKeyboardShowing] = useState(false)
    const [keyboardHeight, setKeyboardHeight] = useState(0)

    useEffect(() => {
        const keyboardShowingListener = Keyboard.addListener('keyboardDidShow', (e) => {
            const tempKeyboardHeight = e.endCoordinates.height
            setKeyboardHeight(tempKeyboardHeight)
            setKeyboardShowing(true)
        })
        const keyboardHidingListener = Keyboard.addListener('keyboardDidHide', () => {
            setKeyboardHeight(0)
            setKeyboardShowing(false)
        })
        return () => {
            keyboardShowingListener.remove()
            keyboardHidingListener.remove()
        }
    }, [])

    const gotoStageTwo = () => setRegistrationStage(2)
    const gotoStageThree = () => setRegistrationStage(3)

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={[{ top: keyboardShowing ? -80 : 0 }]}>
                <Header />
                <View style={registrationStyle}>
                    {registrationStage === 2 ? (
                        <RegistrationCode gotoStageThree={gotoStageThree} />
                    ) : registrationStage === 3 ? (
                        <RegistrationWelcome />
                    ) : (
                        <RegistrationUserData gotoStageTwo={gotoStageTwo} />
                    )}
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}
