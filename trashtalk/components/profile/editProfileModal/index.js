/* eslint-disable */
import React, { useState, useEffect } from 'react'
import { Alert, Modal, TouchableOpacity, View } from 'react-native'

import { _getUserDetails, _updateUserDetails } from '../../../utils/api/userAPI'

import EditProfileHeader from '../subs/editProfileHeader'
import EditProfileDetailText from '../subs/editProfileText'
import EditProfileDetailModal from '../editProfileDetailModal'

import { whiteEx } from '../../../utils/svgs'

import { editProfileModalStyle } from './style'

//how do i make this global...?
const invalidAlert = (title, detail) => {
    return Alert.alert(title, detail, [{ text: 'OK', onPress: () => {} }])
}

export default editProfileModal = ({ modalVisible, hideModal }) => {
    const [detailModalVisible, setDetailModalVisible] = useState(false)
    const [editDetails, setEditDetails] = useState({ submitText: 'Save' })
    const [showUserDetails, setShowUserDetails] = useState(false)
    const [userDetails, setUserDetails] = useState({})

    const hideDetailModal = () => setDetailModalVisible(false)
    const showDetailModal = () => setDetailModalVisible(true)

    const usernameDetail = () => {
        setEditDetails({
            ...editDetails,
            textDetail: userDetails.username,
            titleText: 'Username',
            type: 'username',
        })
        showDetailModal()
    }

    const userBioDetail = () => {
        setEditDetails({
            ...editDetails,
            textDetail: userDetails.bio,
            titleText: 'Bio',
            type: 'bio',
        })
        showDetailModal()
    }

    const loadUserDetails = async () => {
        setShowUserDetails(false)
        //loading screen
        const { username, bio } = await _getUserDetails()
        global.username = username //seems strange but is efficient
        setUserDetails({ username, bio })

        setShowUserDetails(true)
    }

    const updateUserDetails = async ({ username, bio }) => {
        console.log({ username, bio })
        const updateSuccess = await _updateUserDetails({ username, bio })

        if (!updateSuccess) {
            invalidAlert('Invalid Username', 'Someone may already have this username')
        } else {
            hideDetailModal()
            await loadUserDetails()
        }
    }

    useEffect(() => {
        const getUserDetails = async () => {
            await loadUserDetails()
        }

        getUserDetails()
    }, [])

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {}}
        >
            <EditProfileDetailModal
                details={editDetails}
                detailModalVisible={detailModalVisible}
                hideDetailModal={hideDetailModal}
                updateDetail={updateUserDetails}
            />
            <View style={editProfileModalStyle.container}>
                <EditProfileHeader
                    backSvg={whiteEx}
                    titleText={'Profile'}
                    submitText={'Done'}
                    hideModal={hideModal}
                    submitModal={hideModal}
                />
                {showUserDetails && (
                    <>
                        <EditProfileDetailText
                            detailText={userDetails.username}
                            title={'Username'}
                            showDetailModal={usernameDetail}
                        />
                        <EditProfileDetailText
                            detailText={userDetails.bio}
                            title={'Bio'}
                            showDetailModal={userBioDetail}
                        />
                        {/* more user details */}
                    </>
                )}
            </View>
        </Modal>
    )
}
