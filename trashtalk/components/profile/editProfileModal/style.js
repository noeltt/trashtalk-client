/* eslint-disable */
import { StyleSheet } from 'react-native'
import { insetTop, windowWidth, windowHeight } from '../../../utils/constants'

export const editProfileModalStyle = StyleSheet.create({
    container: {
        height: windowHeight,
        width: windowWidth,
        alignItems: 'center',
        //justifyContent: 'center',

        marginTop: insetTop,

        backgroundColor: 'white',
    },
})
