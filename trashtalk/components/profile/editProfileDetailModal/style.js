/* eslint-disable */
import { StyleSheet } from 'react-native'
import { insetTop, windowWidth, windowHeight, mainPaddingLength } from '../../../utils/constants'

export const editProfileDetailModalStyle = StyleSheet.create({
    container: {
        height: windowHeight,
        width: windowWidth,
        alignItems: 'center',
        //justifyContent: 'center',

        marginTop: insetTop,

        backgroundColor: 'white',
    },
    topContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor: 'powderblue'
    },
    detailContainer: {
        paddingTop: 15,
        //height: 300,
        width: '100%',

        // backgroundColor: 'powderblue'
    },
    topTitle: {
        marginLeft: mainPaddingLength,
        // fontFamily: 'LEMON MILK Pro FTR',
        fontSize: 16,
        fontWeight: 'bold',
    },
    textInput: {
        marginTop: 5,
        paddingLeft: mainPaddingLength,
        maxWidth: windowWidth,
    },
})
