/* eslint-disable */
import React, { useState, useEffect } from 'react'
import { Alert, Text, Modal, TextInput, View } from 'react-native'

import EditProfileHeader from '../subs/editProfileHeader'

import { whiteBackArrow } from '../../../utils/svgs'

import { editProfileDetailModalStyle } from './style'

const checkUsername = (username) =>
    !/\s/g.test(username) && username.length < 99 && username.length > 0
const invalidAlert = (title, detail) => {
    return Alert.alert(title, detail, [{ text: 'OK', onPress: () => {} }])
}

const EditTextDetail = ({ textDetailTitle, textDetail, textDetailUpdateFunction }) => {
    const [charLimit, setCharLimit] = useState(100)

    const onChangeText = (text) => {
        textDetailUpdateFunction(text)
    }

    return (
        <View style={editProfileDetailModalStyle.detailContainer}>
            <Text style={editProfileDetailModalStyle.topTitle}>{textDetailTitle}</Text>
            <TextInput
                style={editProfileDetailModalStyle.textInput}
                onChangeText={onChangeText}
                defaultValue={textDetail}
                multiline={true}
            />
        </View>
    )
}

export default editProfileDetailModal = ({
    details,
    hideDetailModal,
    detailModalVisible,
    updateDetail,
}) => {
    const [username, setUsername] = useState('')
    const [bio, setUserBio] = useState('')
    const [textDetailTitle, setTextDetailTitle] = useState('')
    //more details to come

    //team detail, picture details...?
    const { titleText, type, textDetail } = details

    const updateUserDetail = async () => {
        if (type === 'username') {
            console.log({ username })
            if (checkUsername(username)) updateDetail({ username })
            else
                invalidAlert(
                    'Invalid Username',
                    'New username must not be longer than 100 characters or contain spaces or line breaks'
                )
        } else if (type === 'bio') {
            updateDetail({ bio })
        }
    }

    const setUserDetailText = (text) => {
        if (type === 'username') {
            setUsername(text)
        } else if (type === 'bio') {
            setUserBio(text)
        }
    }

    useEffect(() => {
        if (type === 'username') {
            setUsername(textDetail)
            setTextDetailTitle('Username')
        } else if (type === 'bio') {
            setUserBio(textDetail)
            setTextDetailTitle('Bio')
        }
    }, [details])

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={detailModalVisible}
            onRequestClose={() => {}}
        >
            <View style={editProfileDetailModalStyle.container}>
                <EditProfileHeader
                    backSvg={whiteBackArrow}
                    titleText={titleText}
                    submitText={details.submitText}
                    hideModal={hideDetailModal}
                    submitModal={updateUserDetail}
                />
                <EditTextDetail
                    textDetailTitle={textDetailTitle}
                    textDetail={textDetail}
                    textDetailUpdateFunction={setUserDetailText}
                />
            </View>
        </Modal>
    )
}
