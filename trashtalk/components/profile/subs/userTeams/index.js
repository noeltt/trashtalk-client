/* eslint-disable */
import React, { useEffect, useState } from 'react'
import FastImage from 'react-native-fast-image'
import { View, Image, Text, FlatList, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'

import { _getFollowingTeamData, _getProfileFollowingTeamData } from '../../../../utils/api/statsAPI'

import { userTeamStyle } from './style'

export default UserTeams = ({ profileUid }) => {
    const [visible, setVisible] = useState(false)
    const [teams, setTeams] = useState()

    // could be done more gracefully
    const getFollowingTeams = async () => {
        const { teamsData } = await _getFollowingTeamData()
        console.log({ teamsData })
        setTeams(teamsData)
        if (teamsData !== undefined && teamsData.length > 0) setVisible(true)
    }

    const getProfileFollowingTeams = async () => {
        const { teamsData } = await _getProfileFollowingTeamData(profileUid)
        console.log({ teamsData })
        setTeams(teamsData)
        if (teamsData !== undefined && teamsData.length > 0) setVisible(true)
    }

    const renderItem = ({ item }) => <TeamCard teamData={item} />

    useEffect(() => {
        const loadTeams = async () => {
            if (profileUid.length > 0) {
                getProfileFollowingTeams()
            } else {
                getFollowingTeams()
            }
        }
        loadTeams()
    }, [])

    return (
        visible && (
            <View style={userTeamStyle.container}>
                <View style={userTeamStyle.userTeamHeaderContainer}>
                    <Text style={userTeamStyle.userTeamHeaderText}>Teams</Text>
                </View>
                <FlatList
                    horizontal={true}
                    data={teams}
                    renderItem={renderItem}
                    keyExtractor={(item, index) => index}
                />
            </View>
        )
    )
}

const TeamCard = ({ teamData }) => {
    const { logoUrl, teamId } = teamData

    const navigation = useNavigation()

    const goToTeamProfile = () => {
        navigation.push('Home', { screen: 'TeamProfile', params: { teamId } })
    }

    return (
        <TouchableOpacity style={userTeamStyle.userTeamCardContainer} onPress={goToTeamProfile}>
            <FastImage
                style={userTeamStyle.userTeamCardImage}
                source={{
                    uri: logoUrl,
                    priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.contain}
            />
        </TouchableOpacity>
    )
}
