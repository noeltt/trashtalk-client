/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

const teamCardHeight = 70

export const userTeamStyle = StyleSheet.create({
    container: {
        marginLeft: mainPaddingLength,
        marginTop: 15,
        //eight: 80,
        width: windowWidth - mainPaddingLength,

        // borderWidth: 1,
        // borderColor: 'black'
    },
    userTeamHeaderContainer: {
        height: 30,
        width: '100%',
        justifyContent: 'center',
        //backgroundColor: 'pink'
    },
    userTeamHeaderText: {
        //fontSize: "LEMON MILK Pro FTR",
        fontWeight: 'bold',
        fontSize: 18,
    },
    userTeamCardContainer: {
        height: teamCardHeight,
        aspectRatio: 1,
        // backgroundColor: 'gray',
        marginRight: 10,
    },
    userTeamCardImage: {
        height: '100%',
        width: '100%',
    },
})
