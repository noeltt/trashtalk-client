/* eslint-disable */

import React, { useState } from 'react'
import { TouchableOpacity, View, Text } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { whiteEx } from '../../../../utils/svgs'
import { createPostHeaderStyle } from '../../../posts/subs/createPostHeader/style'

export default editProfileHeader = ({ backSvg, submitText, titleText, hideModal, submitModal }) => {
    // let postTextColor = readyToPost ? '#FFB650' : 'gray'
    // let activeOpacity = readyToPost ? .2 : 1
    let postTextColor = '#FFB650'
    let activeOpacity = 0.2

    return (
        <View style={createPostHeaderStyle.container}>
            <TouchableOpacity style={createPostHeaderStyle.closeContainer} onPress={hideModal}>
                <SvgUri style={createPostHeaderStyle.closeSvg} uri={backSvg} />
            </TouchableOpacity>
            <Text style={createPostHeaderStyle.text}>Edit {titleText}</Text>
            <TouchableOpacity
                activeOpacity={activeOpacity}
                style={createPostHeaderStyle.createPostButtonContainer}
                onPress={submitModal}
            >
                <Text
                    style={[createPostHeaderStyle.createPostButtonText, { color: postTextColor }]}
                >
                    {submitText}
                </Text>
            </TouchableOpacity>
        </View>
    )
}
