/* eslint-disable */
import React, { useState } from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import EditProfileModal from '../../editProfileModal'

import { editWhite } from '../../../../utils/svgs'

import { editProfileButtonStyle } from './style'

export default EditProfileButton = () => {
    const [modalVisible, setModalVisible] = useState(false)

    const hideModal = () => setModalVisible(false)
    const showModal = () => setModalVisible(true)

    return (
        <>
            <EditProfileModal modalVisible={modalVisible} hideModal={hideModal} />
            <TouchableOpacity style={editProfileButtonStyle.container} onPress={showModal}>
                <SvgUri style={editProfileButtonStyle.editProfileLogo} uri={editWhite} />
                <Text style={editProfileButtonStyle.editProfileText}>Edit Profile</Text>
            </TouchableOpacity>
        </>
    )
}
