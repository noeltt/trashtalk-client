/* eslint-disable */
import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

export const editProfileButtonStyle = StyleSheet.create({
    container: {
        marginTop: 10,
        height: 35,
        width: windowWidth - 2 * mainPaddingLength,
        marginLeft: mainPaddingLength,
        borderRadius: 10,
        backgroundColor: 'black',
        flexDirection: 'row',

        justifyContent: 'center',
        alignItems: 'center',
    },
    editProfileLogo: {
        maxHeight: '50%',
        aspectRatio: 1,
    },
    editProfileText: {
        marginLeft: 10,
        //fontFamily: "LEMON MILK Pro FTR",
        fontWeight: 'bold',
        fontStyle: 'normal',
        fontSize: 13,
        color: 'white',
    },
})
