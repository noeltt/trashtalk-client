import React from 'react'
import { View, Text } from 'react-native'

import UserPostCard from '../userPostCard'

import { userPostStyle } from './style'

export default UserPosts = ({ posts }) => {
    const postsList = () => {
        return posts.map((post) => {
            return <UserPostCard key={post} />
        })
    }

    return (
        <View style={userPostStyle.container}>
            <View style={userPostStyle.userPostsHeaderContainer}>
                <Text style={userPostStyle.userPostsHeaderText}>Top Posts</Text>
            </View>
            {postsList()}
        </View>
    )
}
