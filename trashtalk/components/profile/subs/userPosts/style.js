import React from 'react'
import { AppState, MaskedViewComponent, StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

export const userPostStyle = StyleSheet.create({
    container: {
        marginLeft: mainPaddingLength,
        marginTop: 15,
        width: windowWidth - 2 * mainPaddingLength,

        // borderWidth: 1,
        // borderColor: 'black'
    },
    userPostsHeaderContainer: {
        height: 30,
        width: '100%',
        justifyContent: 'center',
        //backgroundColor: 'pink'
    },
    userPostsHeaderText: {
        //fontSize: "LEMON MILK Pro FTR",
        fontWeight: 'bold',
        fontSize: 18,
    },
})
