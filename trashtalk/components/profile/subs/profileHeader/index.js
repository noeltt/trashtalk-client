import React from 'react'
import { View, Image, Text, ImageBackground } from 'react-native'
import { SvgUri } from 'react-native-svg'

import '../../../../utils/globalManager'

import BlackGradient from '../../../main/subs/blackGradient'

import { profileHeaderStyle } from './style'

import { adminLogo } from '../../../../utils/svgs'

export default UserProfile = ({ profileUsername }) => {
    const backgroundImageUrl = 'https://trashtalk-post-images.s3.us-east-2.amazonaws.com/image7.png'
    const tempProfileImage =
        'https://trashtalk-assets.s3.us-west-1.amazonaws.com/defaultprofile.png'

    let isAdmin = true

    return (
        <ImageBackground
            style={profileHeaderStyle.container}
            source={{ uri: backgroundImageUrl }}
            resizeMode="contain"
        >
            <BlackGradient>
                <View style={profileHeaderStyle.profileImageContainer}>
                    <Image
                        style={profileHeaderStyle.profileImage}
                        source={{ uri: tempProfileImage }}
                    />
                </View>
                <View style={profileHeaderStyle.profileUsernameContainer}>
                    {isAdmin && (
                        <SvgUri style={profileHeaderStyle.profileLogoSvg} uri={adminLogo} />
                    )}
                    <Text style={profileHeaderStyle.profileUsernameText}>
                        {profileUsername.length > 0 ? profileUsername : global.username}
                    </Text>
                </View>
                {/* <TouchableOpacity style={[profileHeaderStyle.profileEditImageContainer, mainStyles.bgLightGray]}>

                    <SvgUri style={profileHeaderStyle.profileLogoSvg} uri={editBlack}/>

                    <Text style={profileHeaderStyle.profileEditImageText}>
                        Edit Image
                    </Text>
                </TouchableOpacity> */}
            </BlackGradient>
        </ImageBackground>
    )
}
