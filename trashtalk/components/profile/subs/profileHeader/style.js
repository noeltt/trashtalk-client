import { StyleSheet } from 'react-native'
import { profileHeaderHeight, windowWidth } from '../../../../utils/constants'

const profileImageHeight = 80
const profileUsernameHeight = 40

export const profileHeaderStyle = StyleSheet.create({
    container: {
        height: profileHeaderHeight,
        width: windowWidth,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',

        // borderWidth: 2,
        // borderColor: 'blue'
    },
    gradientOverlay: {
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileImageContainer: {
        height: profileImageHeight,
        aspectRatio: 1,

        // borderWidth: 2,
        // borderColor: 'white'
        // backgroundColor: 'powderblue'
    },
    profileImage: {
        height: '100%',
        width: '100%',
    },
    profileUsernameContainer: {
        height: profileUsernameHeight,
        // width: 240,
        marginTop: 5,
        // justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
    },
    profileLogoContainer: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 2,
        backgroundColor: 'gray',
    },
    profileLogoSvg: {
        marginLeft: 10,
        marginRight: 5,
        minHeight: '50%',
        aspectRatio: 1,
    },
    profileUsernameText: {
        // fontFamily: "Lemon milk pro ftr",
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white',
    },
    profileEditImageContainer: {
        height: 30,
        // width: 100,
        position: 'absolute',
        top: '5%',
        right: '5%',
        flexDirection: 'row',
        alignItems: 'center',

        borderRadius: 5,
    },
    profileEditImageText: {
        marginLeft: 5,
        marginRight: 10,
        // fontFamily: "SF Pro Display",
        fontWeight: 'bold',
        fontSize: 14,
    },
})
