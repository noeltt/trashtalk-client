import React from 'react'
import { MaskedViewComponent, StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

export const userBioStyle = StyleSheet.create({
    container: {
        marginLeft: mainPaddingLength,
        marginTop: 15,
        width: windowWidth - 2 * mainPaddingLength,
    },
    bioHeaderContainer: {
        height: 25,
        width: '100%',
        justifyContent: 'center',
        //backgroundColor: 'pink'
    },
    bioHeaderText: {
        //fontSize: "LEMON MILK Pro FTR",
        fontWeight: 'bold',
        fontSize: 18,
    },
    bioText: {
        //fontSize: "LEMON MILK Pro FTR",
        fontWeight: '400',
        fontSize: 12,
    },
})
