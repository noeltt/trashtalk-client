import React from 'react'
import { View, Image, Text, ImageBackground, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { userBioStyle } from './style'

export default UserProfileBio = ({ bio }) => {
    return (
        <View style={userBioStyle.container}>
            <View style={userBioStyle.bioHeaderContainer}>
                <Text style={userBioStyle.bioHeaderText}>Bio</Text>
            </View>
            <Text style={userBioStyle.bioText}>{bio}</Text>
        </View>
    )
}
