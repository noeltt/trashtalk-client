/* eslint-disable */
import { StyleSheet } from 'react-native'
import { headerHeight, mainPaddingLength, windowWidth } from '../../../../utils/constants'

export const editProfileTextStyle = StyleSheet.create({
    container: {
        paddingTop: 15,
        marginBottom: mainPaddingLength,
        width: windowWidth,

        // borderWidth: 1,
        // borderColor: 'black'
    },
    topContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor: 'powderblue'
    },
    topTitle: {
        marginLeft: mainPaddingLength,
        // fontFamily: 'LEMON MILK Pro FTR',
        fontSize: 16,
        fontWeight: 'bold',
    },
    topSubmitButton: {
        position: 'absolute',
        right: mainPaddingLength,
        // backgroundColor: 'orange'
    },
    topSubmitText: {
        // fontFamily: 'SF Pro Display',
        fontSize: 16,
        fontWeight: '400',
        color: '#2D9CDB',
    },
    bottomContainer: {
        marginTop: 10,
        width: '100%',
    },
    bottomText: {
        marginLeft: mainPaddingLength,
        fontSize: 16,
        fontWeight: '400',
    },
})
