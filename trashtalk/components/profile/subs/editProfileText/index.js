/* eslint-disable */
import React, { useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'

import { editProfileTextStyle } from './style'
import { mainStyles } from '../../../../styles/main'

export default EditProfileTextDetail = ({ detailText, title, showDetailModal }) => {
    return (
        <View style={[editProfileTextStyle.container, mainStyles.topBorderLine]}>
            <View style={editProfileTextStyle.topContainer}>
                <Text style={editProfileTextStyle.topTitle}>{title}</Text>
                <TouchableOpacity
                    style={editProfileTextStyle.topSubmitButton}
                    onPress={showDetailModal}
                >
                    <Text style={editProfileTextStyle.topSubmitText}>Edit</Text>
                </TouchableOpacity>
            </View>
            <View style={editProfileTextStyle.bottomContainer}>
                <Text style={editProfileTextStyle.bottomText}>{detailText}</Text>
            </View>
        </View>
    )
}
