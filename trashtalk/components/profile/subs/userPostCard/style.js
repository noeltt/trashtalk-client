import { StyleSheet } from 'react-native'
import { windowWidth, mainPaddingLength } from '../../../../utils/constants'

const cardHeight = 80
const cardWidth = windowWidth - 2 * mainPaddingLength
const rightIconWidth = 50

export const userPostCardStyle = StyleSheet.create({
    container: {
        marginVertical: 5,
        height: cardHeight,
        width: cardWidth,
        flexDirection: 'row',

        backgroundColor: 'white',
    },
    statContainer: {
        height: '100%',
        aspectRatio: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'yellow'
    },
    statIcon: {
        minHeight: '20%',
        aspectRatio: 1,
    },
    statText: {
        marginTop: 5,
        maxWidth: '90%',
        textAlign: 'center',
        // fontFamily: "SF Pro Display",
        fontSize: 10,
        fontWeight: 'bold',
    },
    textContainer: {
        height: '100%',
        width: cardWidth - cardHeight - rightIconWidth,
        justifyContent: 'center',
        alignItems: 'center',
    },
    postText: {
        // fontFamily: "SF Pro Display",
        fontSize: 12,
        fontWeight: '400',
    },
    rightIconContainer: {
        height: '100%',
        width: rightIconWidth,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'yellow'
    },
    rightIcon: {
        minHeight: '20%',
        minWidth: '50%',
    },
})
