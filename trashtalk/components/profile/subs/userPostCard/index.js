import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useNavigation } from '@react-navigation/native'

import { blackForwardArrow, blackComment, blackLike, blackTrash } from '../../../../utils/svgs'

import { mainStyles } from '../../../../styles/main'
import { userPostCardStyle } from './style'

export default UserPostCard = ({ postData }) => {
    // let {statString = "", statType = 0, postString = ""} = postData

    const navigation = useNavigation()

    const checkProfile = () => {
        navigation.navigate('Posts', { someKey: 'someValue' })
    }

    let topStat = 'trash'
    let statIcon = topStat === 'like' ? blackLike : topStat === 'trash' ? blackTrash : blackComment

    return (
        <TouchableOpacity
            style={[userPostCardStyle.container, mainStyles.bgLightGray]}
            onPress={checkProfile}
        >
            <View style={userPostCardStyle.statContainer}>
                <SvgUri style={userPostCardStyle.statIcon} uri={statIcon} />
                <Text style={userPostCardStyle.statText}>900k Trash Takes</Text>
            </View>
            <View style={userPostCardStyle.textContainer}>
                <Text style={userPostCardStyle.postText}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ...
                </Text>
            </View>
            <View style={userPostCardStyle.rightIconContainer}>
                <SvgUri style={userPostCardStyle.rightIcon} uri={blackForwardArrow} />
            </View>
        </TouchableOpacity>
    )
}
