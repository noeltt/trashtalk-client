import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

const userStatsHeight = 50

export const userStatsStyle = StyleSheet.create({
    container: {
        height: userStatsHeight,
        width: windowWidth,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,

        // borderWidth: 1,
        // borderColor: 'black'

        //backgroundColor: "powderblue"
    },
    statsContainer: {
        marginHorizontal: '1.5%',
        height: userStatsHeight,

        // borderWidth: 1,
        // borderColor: 'black'
    },
    statsContainerTop: {
        height: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'pink'
    },
    statsContainerTopText: {
        //fontFamily: "SF Pro Display",
        fontWeight: '700',
        fontStyle: 'normal',
        fontSize: 16,
    },
    statsContainerBottom: {
        height: '40%',
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'yellow'
    },
    statsContainerBottomText: {
        //fontFamily: "SF Pro Display",
        fontWeight: '500',
        fontStyle: 'normal',
        fontSize: 12,
    },
})
