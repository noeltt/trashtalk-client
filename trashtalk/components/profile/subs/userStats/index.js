import React from 'react'
import { View, Text } from 'react-native'

import { userStatsStyle } from './style'

const StatsTile = ({ count = 0, title = '' }) => (
    <View style={userStatsStyle.statsContainer}>
        <View style={userStatsStyle.statsContainerTop}>
            <Text style={userStatsStyle.statsContainerTopText}>{count}</Text>
        </View>
        <View style={userStatsStyle.statsContainerBottom}>
            <Text style={userStatsStyle.statsContainerBottomText}>{title}</Text>
        </View>
    </View>
)

export default UserStats = (props) => {
    return (
        <View style={userStatsStyle.container}>
            <StatsTile count={318} title={'Total Posts'} />
            <StatsTile count={799} title={'Total Likes'} />
            <StatsTile count={100} title={'Trash Takes'} />
            <StatsTile count={11} title={'Total Badges'} />
        </View>
    )
}
