/* eslint-disable */
import React, { useState, useEffect } from 'react'
import { View, ScrollView } from 'react-native'

import Header from '../../../components/main/iaHeader'
import ProfileHeader from '../subs/profileHeader'
import UserStats from '../subs/userStats'
import EditProfileButton from '../subs/editButton'
import UserBio from '../subs/userBio'
import UserTeams from '../subs/userTeams'
import UserPosts from '../subs/userPosts'

import { userProfileStyle } from './style'
import { _getUserDetails, _getUserProfile } from '../../../utils/api/userAPI'

let tempTeams = [1, 2, 3, 4, 5, 6, 7, 8, 9]
let tempPosts = [1, 2, 3]

export default UserProfile = ({ pageData, profileUid }) => {
    let showDetails = true

    const [profileBio, setProfileBio] = useState('')
    const [profileUsername, setProfileUsername] = useState('')
    const [isUserProfile, setIsUserProfile] = useState(false)

    useEffect(() => {
        const getUserData = async () => {
            if (profileUid && profileUid.length > 0) {
                const { bio, username } = await _getUserProfile(profileUid)
                setProfileBio(bio)
                setProfileUsername(username)
            } else {
                setIsUserProfile(true)
                const { bio, username } = await _getUserDetails()
                setProfileBio(bio)
                setProfileUsername(username)
            }
        }
        getUserData()
    }, [])

    return (
        <View style={userProfileStyle.container}>
            <Header />
            <ScrollView style={userProfileStyle.scrollViewContainer}>
                <ProfileHeader profileUsername={profileUsername} />
                {showDetails && <UserStats />}
                {isUserProfile && <EditProfileButton />}
                <UserBio bio={profileBio} />
                <UserTeams profileUid={profileUid} />
                {tempPosts.length > 0 && <UserPosts posts={tempPosts} />}
            </ScrollView>
        </View>
    )
}
