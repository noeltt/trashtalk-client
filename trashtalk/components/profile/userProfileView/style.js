import React from 'react'
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../utils/constants'

export const userProfileStyle = StyleSheet.create({
    container: {
        height: '100%',
        width: windowWidth,
        backgroundColor: 'white',
    },
    scrollViewContainer: {
        width: windowWidth,
        backgroundColor: 'white',
    },
})
