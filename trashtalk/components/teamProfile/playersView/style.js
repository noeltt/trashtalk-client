/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    profileHeaderHeight,
    mainPaddingLength,
    navBarHeight,
    footerHeight,
    headerHeight,
} from '../../../utils/constants'

const teamViewHeight =
    windowHeight - navBarHeight - profileHeaderHeight - footerHeight - headerHeight

const col1Width = '50%'
const col2Width = `${50 / 4}%`

export const teamPlayersViewStyle = StyleSheet.create({
    container: {
        marginTop: 5,
        height: teamViewHeight - 5,
        width: windowWidth - 2 * mainPaddingLength,
        backgroundColor: 'white',
        //borderWidth: .5
    },
    tableHeader: {
        height: 15,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    col1: {
        height: '100%',
        width: col1Width,
        flexDirection: 'row',
        alignItems: 'center',
        //borderWidth: .5
    },
    col2: {
        height: '100%',
        width: col2Width,
        //borderWidth: .5,

        alignItems: 'center',
        justifyContent: 'center',
    },
    headshotImg: {
        height: '100%',
        aspectRatio: 1,
    },
    nameText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '600',
        fontSize: 10,
    },
    statText: {
        // fontFamily: "SF Pro Display",
        fontWeight: '300',
        fontSize: 10,
    },
})
