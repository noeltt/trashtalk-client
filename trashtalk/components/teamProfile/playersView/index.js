/* eslint-disable */

import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import FastImage from 'react-native-fast-image'
import { useNavigation } from '@react-navigation/native'

import { _getNBATeamPlayers } from '../../../utils/api/statsAPI'

import StatsTableRow from '../../main/subs/statsTableRow'

import { teamPlayersViewStyle } from './style'

export default teamPlayersView = ({ teamId }) => {
    const [teamPlayers, setTeamPlayers] = useState([])
    const navigation = useNavigation()

    useEffect(() => {
        console.log({ teamId })
        const getPlayers = async () => {
            const ret = await _getNBATeamPlayers(teamId)

            if (ret) {
                const { players } = ret
                setTeamPlayers(players)
                // console.log({ teamPlayers })
            } else {
                console.log('client error getting team players')
            }
        }

        getPlayers()
    }, [])

    const renderItem = ({ item, index }) => {
        const {
            id,
            headshotUrl,
            firstName,
            lastName,
            positionShort,
            gamesPlayed,
            pointsPerGame,
            assistsPerGame,
            reboundsPerGame,
        } = item
        let fullName = firstName + ' ' + lastName
        fullName = fullName.length > 16 ? `${fullName[0]}. ${lastName}` : fullName

        if (positionShort && positionShort.length > 0) fullName += ` - ${positionShort}`

        const gotoTeamPlayerProfile = () => {
            navigation.push('Home', { screen: 'TeamPlayerProfile', params: { playerId: id } })
        }

        return (
            <StatsTableRow colorIndex={index % 2}>
                <TouchableOpacity style={teamPlayersViewStyle.col1} onPress={gotoTeamPlayerProfile}>
                    <FastImage
                        style={teamPlayersViewStyle.headshotImg}
                        source={{
                            uri: headshotUrl,
                            priority: FastImage.priority.normal,
                        }}
                        resizeMode={FastImage.resizeMode.contain}
                    />
                    <Text style={teamPlayersViewStyle.nameText}>{fullName}</Text>
                </TouchableOpacity>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.statText}>{gamesPlayed}</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.statText}>{pointsPerGame}</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.statText}>{assistsPerGame}</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.statText}>{reboundsPerGame}</Text>
                </View>
            </StatsTableRow>
        )
    }

    return (
        <View style={teamPlayersViewStyle.container}>
            <View style={teamPlayersViewStyle.tableHeader}>
                <View style={teamPlayersViewStyle.col1}></View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.nameText}>GP</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.nameText}>PTS</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.nameText}>AST</Text>
                </View>
                <View style={teamPlayersViewStyle.col2}>
                    <Text style={teamPlayersViewStyle.nameText}>TREB</Text>
                </View>
            </View>
            <FlatList
                data={teamPlayers}
                renderItem={renderItem}
                keyExtractor={(item, index) => index}
            />
        </View>
    )
}
