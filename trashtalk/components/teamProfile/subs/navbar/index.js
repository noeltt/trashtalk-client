/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import { gameNavStyle } from '../../../game/subs/gameRoomNav/style'

export default teamProfileNavBar = ({ navigateView }) => {
    const changeView = (viewType) => navigateView(viewType)

    return (
        <View style={gameNavStyle.container}>
            <TouchableOpacity
                style={gameNavStyle.navButtonContainer}
                onPress={() => {
                    changeView(1)
                }}
            >
                <Text style={gameNavStyle.navText}>Posts</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={gameNavStyle.navButtonContainer}
                onPress={() => {
                    changeView(2)
                }}
            >
                <Text style={gameNavStyle.navText}>Players</Text>
            </TouchableOpacity>
            <TouchableOpacity style={gameNavStyle.navButtonContainer}>
                <Text style={gameNavStyle.navText}>Schedule</Text>
            </TouchableOpacity>
        </View>
    )
}
