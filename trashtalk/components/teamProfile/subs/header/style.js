/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    profileHeaderHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
    headerHeight,
} from '../../../../utils/constants'

export const teamHeaderStyle = StyleSheet.create({
    container: {
        height: profileHeaderHeight,
        width: windowWidth,

        backgroundColor: 'black',
    },
    teamLogo: {
        height: '50%',
        aspectRatio: 1,
        //backgroundColor: 'blue'
    },
    teamName: {
        //fontFamily: 'LEMON MILK Pro FTR',
        marginTop: '3%',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
    },
})
