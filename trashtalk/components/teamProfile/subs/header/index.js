/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, Text, FlatList, ImageBackground } from 'react-native'
import FastImage from 'react-native-fast-image'

import BlackGradient from '../../../main/subs/blackGradient'

import { teamHeaderStyle } from './style'

export default teamProfileHeader = ({ teamLogo, teamBackground, teamName }) => {
    // const backgroundImageUrl = 'https://trashtalk-post-images.s3.us-east-2.amazonaws.com/image7.png'
    // const teamTeamImage = 'https://upload.wikimedia.org/wikipedia/fr/e/ee/Hawks_2016.png'
    // const teamName = 'Atlanta Hawks'

    return (
        <ImageBackground
            style={teamHeaderStyle.container}
            source={{ uri: teamBackground }}
            resizeMode="cover"
        >
            <BlackGradient>
                <FastImage
                    style={teamHeaderStyle.teamLogo}
                    source={{
                        uri: teamLogo,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <Text style={teamHeaderStyle.teamName}>{teamName}</Text>
            </BlackGradient>
        </ImageBackground>
    )
}
