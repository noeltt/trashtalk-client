/* eslint-disable */
import { StyleSheet } from 'react-native'
import {
    windowHeight,
    windowWidth,
    profileHeaderHeight,
    mainPaddingLength,
    navBarHeight,
    footerHeight,
    headerHeight,
} from '../../../utils/constants'

const teamViewHeight =
    windowHeight - navBarHeight - profileHeaderHeight - footerHeight - headerHeight

export const teamPostsViewStyle = StyleSheet.create({
    container: {
        height: teamViewHeight,
        paddingLeft: mainPaddingLength,
    },
    postsContainer: {
        width: windowWidth - mainPaddingLength,
    },
})
