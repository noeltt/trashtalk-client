/* eslint-disable */
import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { StatusBar } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import SplashScreen from './screens/splash'
import LoginScreen from './screens/login'
import HomeScreen from './screens/home'
import Profile from './screens/profile'
import Search from './screens/search'
import LiveChat from './screens/livechat'
import TabBar from './components/main/subs/tabBar'
import Registration from './screens/registration'
import Posts from './screens/posts'
import TeamProfile from './screens/teamProfile'
import TeamPlayerProfile from './screens/teamPlayerProfile'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { SafeAreaView } from 'react-native-safe-area-context'

const MainStack = createBottomTabNavigator()
const RegistrationStack = createNativeStackNavigator()
const Stack = createNativeStackNavigator()
// tabBar={props => <TabBar {...props}/>}

function Home() {
    return (
        <>
            <SafeAreaView
                style={{
                    flex: 0,
                    paddingBottom: -20,
                    backgroundColor: 'black',
                    // borderWidth: 3,
                    // borderColor: 'blue',
                }}
            />
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: 'white',
                    marginTop: -10,
                    paddingTop: -50,
                    // borderWidth: 2,
                    // borderColor: 'red',
                }}
            >
                <StatusBar barStyle="light-content" />
                <MainStack.Navigator
                    initialRouteName="HomeScreen"
                    backBehavior="history"
                    tabBar={(props) => <TabBar {...props} />}
                >
                    <MainStack.Screen
                        name="HomeScreen"
                        component={HomeScreen}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="LiveChat"
                        component={LiveChat}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="Search"
                        component={Search}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="Profile"
                        component={Profile}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="Posts"
                        component={Posts}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="TeamProfile"
                        component={TeamProfile}
                        options={{ headerShown: false }}
                    />
                    <MainStack.Screen
                        name="TeamPlayerProfile"
                        component={TeamPlayerProfile}
                        options={{ headerShown: false }}
                    />
                </MainStack.Navigator>
            </SafeAreaView>
        </>
    )
}

const RegistrationFlow = () => (
    <SafeAreaView style={{ flex: 1 }}>
        <RegistrationStack.Navigator initialRouteName="Register">
            <Stack.Screen
                name="Register"
                component={Registration}
                options={{ headerShown: false }}
            />
        </RegistrationStack.Navigator>
    </SafeAreaView>
)

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen
                    name="Splash"
                    component={SplashScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen
                    name="Registration"
                    component={RegistrationFlow}
                    options={{ headerShown: false }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default App
