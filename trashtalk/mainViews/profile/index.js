/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, ScrollView, Text } from 'react-native'
import { mainStyles } from '../../styles/main'

import UserProfile from '../../components/profile/userProfileView'

export default Profile = ({ pageData, profileUid }) => {
    return (
        <View style={mainStyles.mainScrollView}>
            <UserProfile profileUid={profileUid} />
        </View>
    )
}
