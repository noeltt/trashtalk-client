/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, ScrollView, Text } from 'react-native'

import Header from '../../components/main/iaHeader'
import { SearchLobby } from '../../components/search/searchHome'

import { searchStyle } from './style'

export default Search = (props) => {
    return (
        <View style={searchStyle.container}>
            <Header />
            <SearchLobby />
        </View>
    )
}
