import React from 'react'
import { StyleSheet } from 'react-native'
import { mainStyles } from '../../styles/main'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
} from '../../utils/constants'

const rowHeight = 150

export const searchStyle = StyleSheet.create({
    container: {
        height: '100%',
    },
})
