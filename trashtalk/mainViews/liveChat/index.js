/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, ScrollView, Text } from 'react-native'

import { mainStyles } from '../../styles/main'
import Header from '../../components/main/iaHeader'
import Lobby from '../../components/game/lobby'
import GameRoom from '../../components/game/gameRoom'

const logoUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/logoblack.svg'

export default LiveChat = (props) => {
    const [gameId, setGameId] = useState(props.gameId)
    const { viewParam } = props
    // console.log({ viewParam })

    gotoChatroom = (newGameId) => {
        props.updateViewParam()
        setGameId(newGameId)
    }

    const ChatView = viewParam === 'default' ? Lobby : gameId > 0 ? GameRoom : Lobby

    return (
        <View style={[mainStyles.bgWhite]}>
            <Header />
            <ChatView gotoChatroom={gotoChatroom} gameId={gameId} />
        </View>
    )
}
