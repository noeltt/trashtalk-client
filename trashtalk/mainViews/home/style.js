import React from 'react'
import { StyleSheet } from 'react-native'
import { mainStyles } from '../../styles/main'
import {
    windowHeight,
    windowWidth,
    footerHeight,
    devGap,
    mainPaddingLength,
} from '../../utils/constants'

const rowHeight = 150

export const homeStyle = StyleSheet.create({
    container: {
        height: '100%', //- (footerHeight),
        backgroundColor: 'white',
    },
    featuredContainer: {
        height: 300,
        width: windowWidth,

        // borderWidth: 1,
        // borderColor: 'black',
        //backgroundColor: 'skyblue'
    },
    featuredContainerImage: {
        height: '100%',
        width: '100%',
        resizeMode: 'cover',
        //justifyContent: 'center',
        //alignItems: 'center'
    },
    featuredTextContainer: {
        height: 50,
        width: '90%',
        marginTop: '40%',
        marginLeft: mainPaddingLength,

        justifyContent: 'center',

        //backgroundColor: 'black'
    },
    featuredText: {
        //fontFamily: 'LEMON MILK Pro FTR',
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
    },
    featuredButton: {
        height: 35,
        width: '35%',

        paddingLeft: '3%',
        marginLeft: mainPaddingLength,
        marginTop: '2%',
        borderRadius: 5,

        alignItems: 'center',
        flexDirection: 'row',
    },
    featureButtonText: {
        //fontFamily: 'LEMON MILK Pro FTR',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black',
    },
    featureButtonIconContainer: {
        height: '100%',
        aspectRatio: 1,
        position: 'absolute',
        right: 5,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'yellow'
    },
    featureButtonIcon: {
        minHeight: '50%',
        minWidth: '50%',
    },
    gridContainer: {
        paddingLeft: mainPaddingLength,
        width: windowWidth,
        //borderWidth: 2,
        // borderColor: 'blue',
        backgroundColor: 'white',
    },
    gridHeader: {
        flexDirection: 'row',
        height: 60,
        width: '100%',
        marginTop: 5,
        // borderWidth: 2,
        // borderColor: 'blue',
        backgroundColor: 'white',
    },
    gridBannerTextContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%',
        width: '50%',
        marginRight: 0,
        // borderColor: 'red',
        // backgroundColor: 'powderblue',
        marginLeft: 0,
    },
    gridBannerDDButton: {
        height: '100%',
        width: 60,
        position: 'absolute',
        right: 10,
        marginRight: 0,
        backgroundColor: 'skyblue',
    },
    gridBanner: {
        justifyContent: 'center',
        height: 50,
        width: '100%',
        // borderWidth: 2,
        // borderColor: 'blue',
        // backgroundColor: 'white'
    },
    gridContentContainer: {},
    gridRow: {
        height: rowHeight,
        maxHeight: rowHeight,
        flexDirection: 'row',
        marginBottom: 10,
        //borderWidth: 2,
        // borderColor: 'red',
        // backgroundColor: 'tra'
    },
    gridCell: {
        height: rowHeight,
        width: rowHeight,
        marginRight: 20,
        // borderWidth: 2,
        // borderColor: 'red',
        backgroundColor: 'gray',
    },
})
