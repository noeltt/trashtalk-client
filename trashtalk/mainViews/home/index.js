/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList, ImageBackground } from 'react-native'
import { SvgUri } from 'react-native-svg'
import { useNavigation } from '@react-navigation/native'

import SubHeaderOne from '../../components/main/subs/headerOne'
import SubHeaderOneButton from '../../components/main/subs/headerOneButton'
import GridCell from '../../components/main/subs/gridCell'
import CreatePostModal from '../../components/posts/createPostModal'

import { homeStyle } from './style'
import { mainStyles } from '../../styles/main'

import { _getRecentPosts } from '../../utils/api/postAPI'

const subHeaderTitle = 'Posts'
const postArrowImageUrl = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/featuredarrow.svg'

const testImageUrl = 'https://trashtalk-post-images.s3.us-east-2.amazonaws.com/image4.png'

// npm react native sideswipe
export default HomeScreen = () => {
    const navigation = useNavigation()

    const [latestPosts, setLatestPosts] = useState([])
    const [modalVisible, setModalVisible] = useState(false)

    useEffect(() => {
        console.log('GET POST AT HOME CALL')
        const getHomePosts = async () => {
            const { posts } = await _getRecentPosts()
            setLatestPosts(posts)
        }

        getHomePosts()
    }, [])

    const goToPostTest = () => {
        navigation.navigate('Posts', { postId: 4 })
    }

    const renderItem = ({ item }) => <GridCell postData={item} />

    const hideModal = () => {
        setModalVisible(false)
    }

    const showModal = () => {
        setModalVisible(true)
    }

    return (
        <View style={homeStyle.container}>
            <CreatePostModal modalVisible={modalVisible} hideModal={hideModal} />
            <ScrollView contentContainerStyle={mainStyles.mainScrollView}>
                <View style={homeStyle.featuredContainer}>
                    <ImageBackground
                        source={{ uri: testImageUrl }}
                        style={homeStyle.featuredContainerImage}
                    >
                        <View style={homeStyle.featuredTextContainer}>
                            <Text style={homeStyle.featuredText}>Top NBA Free Agents 2021</Text>
                        </View>
                        <TouchableOpacity
                            style={[homeStyle.featuredButton, mainStyles.bgLightGray]}
                            onPress={showModal}
                        >
                            <Text style={homeStyle.featureButtonText}>See Post</Text>
                            <View style={homeStyle.featureButtonIconContainer}>
                                <SvgUri
                                    style={homeStyle.featureButtonIcon}
                                    uri={postArrowImageUrl}
                                />
                            </View>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={homeStyle.gridContainer}>
                    <SubHeaderOne title={subHeaderTitle}>
                        <SubHeaderOneButton />
                    </SubHeaderOne>
                    <View style={homeStyle.gridBanner}>
                        <Text style={mainStyles.textSizeTwo}>Latest</Text>
                    </View>
                    <FlatList
                        horizontal={true}
                        data={latestPosts}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => item.postId}
                        style={homeStyle.gridRow}
                    />

                    <View style={homeStyle.gridBanner}>
                        <Text style={mainStyles.textSizeTwo}>Greatest</Text>
                    </View>
                    <FlatList
                        horizontal={true}
                        data={latestPosts}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => item.postId}
                        style={homeStyle.gridRow}
                    />
                    <View style={homeStyle.gridBanner}>
                        <Text style={mainStyles.textSizeTwo}>Trending Players</Text>
                    </View>
                    <FlatList
                        horizontal={true}
                        data={latestPosts}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => item.postId}
                        style={homeStyle.gridRow}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
