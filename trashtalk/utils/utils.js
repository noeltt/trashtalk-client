/* eslint-disable */
import { Alert } from 'react-native'

export const getCountText = (count) => {
    if (count < 1000) return count

    if (count > 1000 && count <= 10000) {
        let K = Math.floor(count / 1000)
        let H = Math.floor((count - K * 1000) / 100)
        return `${K}.${H}K`
    }

    if (count > 10000 && count <= 1000000) {
        let K = Math.floor(count / 1000)
        return `${K}K`
    }

    if (count > 1000000 && count <= 10000000) {
        let M = Math.floor(count / 1000000)
        let K = Math.floor((count - M * 1000000) / 100000)
        return `${M}.${K}M`
    }

    if (count > 10000000) {
        let M = Math.floor(count / 1000000)
        return `${M}M`
    }
}

// converts UTC to local 24:60 time
export const getLocalTimeText = (utcDate) => {
    const fDate = new Date(utcDate)

    let hours = fDate.getHours()
    const minutes = fDate.getMinutes()

    const minutesText = String(minutes).length === 1 ? `0${minutes}` : `${minutes}`

    const ampm = hours >= 12 ? 'PM' : 'AM'
    hours = hours % 12
    hours = hours ? hours : 12 // the hour '0' should be '12'

    const timeText = `${hours}:${minutesText}${ampm}`
    return timeText
}

// converts minutes to time text
export const getTimeText = (min) => {
    if (min === 0) return 'now'

    let hr = 60
    let day = 24 * hr
    let week = 7 * day
    let month = 30 * day
    let year = 365 * day

    if (Math.floor(min / year) > 1) return `${Math.floor(min / year)}y ago`

    if (Math.floor(min / month) > 1) return `${Math.floor(min / month)} mo ago`

    if (Math.floor(min / week) > 1) return `${Math.floor(min / week)} weeks ago`

    if (Math.floor(min / day) > 1) return `${Math.floor(min / day)} days ago`

    if (Math.floor(min / hr) > 1) return `${Math.floor(min / hr)} hours ago`

    if (Math.floor(min) > 0) return `${Math.floor(min)} min ago`
}

export const zeroIfLess = (value) => (value > 0 ? value : 0)

export const getQuarterText = (quarter) =>
    quarter === 1
        ? '1st'
        : quarter === 2
        ? '2nd'
        : quarter === 3
        ? '3rd'
        : quarter === 4
        ? '4th'
        : 'TBD'

export const invalidAlert = (title, detail) =>
    Alert(title, detail, [{ text: 'OK', onPress: () => {} }])

export const isEmailValid = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
}
