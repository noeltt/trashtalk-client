/* eslint-disable */
import './globalManager'

const serverUrl = global.apiUrl || 'http://localhost:3003'
const chatUrl = global.chatUrl || 'http://localhost:3000'

// const setHeaders = () => {
//     // do something for JSON
// }

export const setOptions = (method, data) => ({
    method,
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
        'trashtalk-token': global.apiToken,
    },
})

export const setRegistrationOptions = (method, data) => ({
    method,
    body: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
        'trashtalk-token': global.registrationToken,
    },
})

export const sendApiCall = async (options, route) => {
    const serverRoute = serverUrl + route

    // console.log(`trying ${serverRoute}`)

    try {
        const res = await fetch(serverRoute, options)
        const data = await res.json()
        if (!data.success) throw { message: 'invalid return from the api' }
        return data
    } catch (err) {
        console.log(`ERROR WITH ${route}`)
        console.log({ err })
        return false
    }
}

export const sendChatCall = async (options, route) => {
    const serverRoute = chatUrl + route

    // console.log(`trying ${serverRoute}`)

    try {
        const res = await fetch(serverRoute, options)
        const data = await res.json()
        if (!data.success) throw { message: 'invalid return from the chat' }
        return data
    } catch (err) {
        console.log(`ERROR WITH ${route}`)
        console.log({ err })
        return false
    }
}

export const getText = async () => {
    try {
        let route = `${serverUrl}/health`
        let res = await fetch(route, { method: 'GET' })
        const json = await res.json()
        return json
    } catch (e) {
        console.error(e)
        return 'no'
    }
}

// START CHANGING HERE
export const userLogin = async (email, password) => {
    let data = { email, password }
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }
    try {
        let route = `${serverUrl}/login`
        let res = await fetch(route, options)
        const json = await res.json()
        if (json.success) {
            global.apiToken = json.token
            global.username = json.username
        }
        return json.success
    } catch (e) {
        console.error(e)
        return false
    }
}
