import { Dimensions, Platform } from 'react-native'
import { initialWindowMetrics } from 'react-native-safe-area-context'
// const insets = useSafeAreaInsets();

const { insets } = initialWindowMetrics
const insetTop = Platform.OS === 'ios' ? insets.top : 0
const insetHeightDiff = Platform.OS === 'ios' ? insets.top + insets.bottom : 0

const windowWidth = Dimensions.get('window').width
const windowHeight = Dimensions.get('window').height - insetHeightDiff
const footerHeight = 70
const headerHeight = 60
const mainPaddingLength = 20

// profiles
const profileHeaderHeight = 160

// gameRoom
const scoreboardBottomGap = 5
const scoreboardHeight = 80
const navBarHeight = 46
const gameRoomHeaderHeight = scoreboardBottomGap + scoreboardHeight + navBarHeight
const textInputHeight = 40
const devGap = 0

const textInputPlaceholder = 'Talk Your Sh*t...'
// const gameRoomTopGap = 100

export {
    profileHeaderHeight,
    windowWidth,
    textInputPlaceholder,
    devGap,
    windowHeight,
    footerHeight,
    mainPaddingLength,
    textInputHeight,
    headerHeight,
    insetTop,
    scoreboardBottomGap,
    scoreboardHeight,
    navBarHeight,
    gameRoomHeaderHeight,
}
