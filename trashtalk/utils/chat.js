import io from 'socket.io-client'
import './globalManager'

const url = global.chatUrl || 'http://localhost:3000'

exports.initialize = () => {
    // error handling if
    this.socket = io(url)
    return this.socket
}

exports.joinRoom = (gameId) => {
    this.socket.emit('join_room', { gameId })
}

exports.leaveRoom = (gameId) => {
    this.socket.emit('leave_room', { gameId })
}

exports.postChat = (data) => {
    this.socket.emit('post_chat', data)
}

exports.typing = (data) => {
    this.socket.emit('typing', data)
}

exports.spamEmoji = (data) => {
    this.socket.emit('post_emoji', data)
}

exports.socketDisconnect = (gameId) => {
    this.socket.emit('leave_room', { gameId })
    // this.socket.disconnect()
    this.socket = null
    console.log('socket disconnect!')
}
