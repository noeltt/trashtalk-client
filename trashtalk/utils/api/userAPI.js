/* eslint-disable */
import '../globalManager'
import { setOptions, sendApiCall, setRegistrationOptions } from '../apiUtil'

let serverUrl = global.apiUrl || 'http://localhost:3003'
let userId = global.userId || 1

export const _createUser = async (data) =>
    sendApiCall(setRegistrationOptions('POST', data), '/users')
export const _verifyRegistrationCode = async (data) =>
    sendApiCall(setRegistrationOptions('PUT', data), '/code/registration')

export const _getUsersByUsername = async (searchText) =>
    sendApiCall(setOptions('GET'), `/users?username=${searchText}`)
export const _getUserDetails = async () => sendApiCall(setOptions('GET'), '/users/details')
export const _updateUserDetails = async (data) =>
    sendApiCall(setOptions('PUT', data), '/users/details')

export const _followUser = async (data) => sendApiCall(setOptions('POST', data), '/users/following')
export const _unfollowUser = async (data) =>
    sendApiCall(setOptions('DELETE', data), '/users/following')
export const _getUserProfile = async (uid) =>
    sendApiCall(setOptions('GET'), `/users/profile?uid=${uid}`)
