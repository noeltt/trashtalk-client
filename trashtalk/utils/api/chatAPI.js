/* eslint-disable */
import { setOptions, sendChatCall } from '../apiUtil'

export const _getGameChatMessages = async (gameId) =>
    sendChatCall(setOptions('GET'), `/chats/${gameId}`)
