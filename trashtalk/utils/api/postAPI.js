import { setOptions, sendApiCall } from '../apiUtil'

export const _getRecentPosts = async () => sendApiCall(setOptions('GET'), '/posts/latest')
export const _getPosts = async (urlQuery) => sendApiCall(setOptions('GET'), `/posts?${urlQuery}`)

// POST SPECIFIC
export const _createNewPost = async (data) => sendApiCall(setOptions('POST', data), '/posts')
export const _getPostDetails = async (postId) => sendApiCall(setOptions('GET'), `/posts/${postId}`)

export const _likePost = async (postId) =>
    sendApiCall(setOptions('POST', { postId }), `/posts/${postId}/like`)
export const _unlikePost = async (postId) =>
    sendApiCall(setOptions('PUT', { postId }), `/posts/${postId}/unlike`)
export const _trashPost = async (postId) =>
    sendApiCall(setOptions('POST', { postId }), `/posts/${postId}/trash`)
export const _untrashPost = async (postId) =>
    sendApiCall(setOptions('PUT', { postId }), `/posts/${postId}/untrash`)
export const _votePostPoll = async (postId, optionId) =>
    sendApiCall(setOptions('POST', { optionId }), `/posts/${postId}/votes`)

// COMMENT SPECIFIC
export const _createPostComment = async (postId, message) =>
    sendApiCall(setOptions('POST', { postId, message }), `/posts/${postId}/comments`)
export const _getPostComments = async (postId) =>
    sendApiCall(setOptions('GET'), `/posts/${postId}/comments`)

export const _likePostComment = async (commentId) =>
    sendApiCall(setOptions('POST', { commentId }), `/comments/${commentId}/like`)
export const _unlikePostComment = async (commentId) =>
    sendApiCall(setOptions('PUT', { commentId }), `/comments/${commentId}/unlike`)
export const _trashPostComment = async (commentId) =>
    sendApiCall(setOptions('POST', { commentId }), `/comments/${commentId}/trash`)
export const _untrashPostComment = async (commentId) =>
    sendApiCall(setOptions('PUT', { commentId }), `/comments/${commentId}/untrash`)
