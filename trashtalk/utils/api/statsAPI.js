import { setOptions, sendApiCall } from '../apiUtil'

export const _getNBATeams = async () => sendApiCall(setOptions('GET'), '/teams')
export const _getNBATeamDetails = async (teamId) =>
    sendApiCall(setOptions('GET'), `/teams/${teamId}`)

export const _getNBATeamPlayers = async (teamId) =>
    sendApiCall(setOptions('GET'), `/teams/${teamId}/players`)
export const _getNBAPlayerData = async (playerId) =>
    sendApiCall(setOptions('GET'), `/players/${playerId}`)

export const _getFollowingTeamData = async () => sendApiCall(setOptions('GET'), '/teams/follow')
export const _getProfileFollowingTeamData = async (uid) =>
    sendApiCall(setOptions('GET'), `/teams/profile?uid=${uid}`)

export const _followNBATeam = async (teamId) =>
    sendApiCall(setOptions('POST', {}), `/teams/${teamId}/follow`)
export const _unfollowNBATeam = async (teamId) =>
    sendApiCall(setOptions('PUT', {}), `/teams/${teamId}/unfollow`)

export const _getUpcomingGames = async () => sendApiCall(setOptions('GET'), '/games/upcoming')
export const _getGameData = async (gameId) => sendApiCall(setOptions('GET'), `/games/${gameId}`)
export const _getGameLinks = async (gameId) => sendApiCall(setOptions('GET'), `/games/${gameId}/links`)
