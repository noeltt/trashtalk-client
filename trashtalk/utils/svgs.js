const logoWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/logo.svg'
const logoTextWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/trash_talk.svg'

const whiteComment = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whitecomment.svg'
const whiteLike = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whiteWhitelike.svg'
const clearLike = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whitelike.svg'
const whiteTrash = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whiteWhitetrash.svg'
const clearTrash = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whitetrash.svg'
const blackComment = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/blackcomment.svg'
const blackLike = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/blacklike.svg'
const blackTrash = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/blacktrash.svg'

const homeWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/home-5-line.svg'
const chatWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/question-answer-line.svg'
const searchWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/search-line.svg'
const profileWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/user-3-line.svg'
// const homeBlack = ''
const homeBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/home-5-fill.svg'
const chatBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/question-answer-fill.svg'
const searchBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/search-fill.svg'
const profileBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/user-3-fill.svg'

const whiteBackArrow = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whitebackarrow.svg'
const blackForwardArrow = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/rightarrow.svg'

const adminLogo = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/adminlogo.svg'
const fireLogo = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/fire.svg'
const postDiscussionLogo = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/discussion.svg'
const postPollLogo = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/postLabelLogo.svg'
const inputText = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/inputtext.svg'
const viewsLogo = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/views.svg'

const editBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/editblack.svg'
const editWhite = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/editwhite.svg'
const whiteEx = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whiteEx.svg'
const whitePlus = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/whitePlus.svg'

const followersBlack = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/followersBlack.svg'
const redMinus = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/redminus.svg'
const greenPlus = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/greenplus.svg'

const blueTrash = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/blueTrash.svg'
const redHeart = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/redHeart.svg'
const testTeam = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/testTeam.svg'

const regVerify = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/reg_verify.svg'
const regWelcome = 'https://trashtalk-assets.s3.us-west-1.amazonaws.com/reg_welcome.svg'

export {
    logoWhite,
    logoTextWhite,
    clearLike,
    clearTrash,
    whiteComment,
    whiteLike,
    whiteTrash,
    blackComment,
    blackLike,
    blackTrash,
    homeWhite,
    chatWhite,
    searchWhite,
    profileWhite,
    homeBlack,
    chatBlack,
    searchBlack,
    profileBlack,
    whiteBackArrow,
    blackForwardArrow,
    adminLogo,
    fireLogo,
    postPollLogo,
    postDiscussionLogo,
    inputText,
    viewsLogo,
    editBlack,
    editWhite,
    whiteEx,
    whitePlus,
    followersBlack,
    redMinus,
    greenPlus,
    blueTrash,
    redHeart,
    testTeam,
    regVerify,
    regWelcome,
}
